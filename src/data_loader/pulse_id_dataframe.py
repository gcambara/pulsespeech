"""This class allows creation of data containers specialized to input and extract data from Pulse ID repository."""
import pandas as pd
import numpy as np
np.set_printoptions(threshold=np.inf)
import os
import pyphen
import torch
from scipy import ndimage
from scipy import signal

class PulseID_Dataframe:
    def __init__(self, args, use_pickle=True):
        # Constants' initializations.
        self.PPG_TIME_COLUMN = "t(s)"
        self.PPG_VOLTAGE_COLUMN = "V(V)"
        self.PPG_SEPARATOR = "\t"

        self.LAB_INITIAL_TIME_COLUMN = "t0(s)"
        self.LAB_FINAL_TIME_COLUMN = "t1(s)"
        self.LAB_LABEL_COLUMN = "Label"
        self.LAB_SEPARATOR = " "

        self.PULSEID_SAMPLE_COLUMN = "Sample"
        self.PULSEID_LABEL_COLUMN = "Label"
        self.PULSEID_TIME_COLUMN = "Time (s)"
        self.PULSEID_WORDS_COLUMN = "Words"
        self.PULSEID_FILEPATH_COLUMN = "File path"
        self.PULSEID_SAMPLE_ID_COLUMN = "Sample ID"
        self.PULSEID_SUBJECT_ID_COLUMN = "Subject_ID"
        self.PULSEID_SESSION_ID_COLUMN = "Session_ID"
        self.PULSEID_EXPERIMENT_ID_COLUMN = "Experiment_ID"
        self.PULSE_ID_LANGUAGE_COLUMN = "Language"
        self.PULSE_ID_AUDIO_STATUS = "Audio_Status"
        self.PULSE_ID_GENDER = "Gender"
        self.PULSE_ID_SYLLABLES_COUNT = "Syllables_Count"
        self.PULSE_ID_DURATION = "Duration (s)"
        self.PULSE_ID_PROSODY_POSITION = "Prosody Position"

        self.EXPERIMENT_CREDIT_CARD_NUMBERS = "1"
        self.EXPERIMENT_SILENCE = "2"
        self.EXPERIMENT_PIN_NUMBERS = "3"
        self.EXPERIMENT_ASR_SENTENCES = "4"

        self.NONSPEECH_LABEL = "Non-speech"

        self.SAMPLES_PER_SECOND = 200

        # Variables' initializations.
        self.pulseID_pathdir = args.dataset_path
        self.pulseID_ppg_pathdir = self.pulseID_pathdir + "ppg/"
        self.pulseID_lab_pathdir = self.pulseID_pathdir + "lab/"

        self.check_folder_structure()
        self.lab_filepaths = self.store_lab_filepaths()
        self.ppg_filepaths = self.store_ppg_filepaths()
        self.lab_ppg_dict = self.create_lab_ppg_dictionary()

        # Some filters for known properties of Pulse ID database.
        self.experiment_id_whitelist = set(['1', '2', '3', '4'])
        self.session_id_whitelist = set(['SS1'])
        self.subject_id_blacklist = set(['S000'])
        self.language_sensitive_experiment_ids = set(['1', '3'])
        self.catalan_speaker_ids = set(['S001', 'S004', 'S005', 'S011', 'S014', 'S017', 'S018'])
        self.noisy_wavs_ids = set(['S022', 'S023', 'S024', 'S025', 'S026', 'S027', 'S028', 'S029', 'S030'])
        self.male_speaker_ids = set(['S001', 'S002', 'S003', 'S004', 'S005', 'S006', 'S009', 'S011', 'S012', 'S013', 'S014', 'S015', 'S016', 'S017', 'S019', 'S021', 'S022', 'S023', 'S024', 'S025', 'S026', 'S027', 'S028', 'S029', 'S031'])
        self.female_speaker_ids = set(['S007', 'S008', 'S010', 'S018', 'S020', 'S030'])
        self.one_syllable_words = set(['dos', 'tres', 'seis'])
        self.one_syllable_catalan_words = set(['uno', 'dos', 'tres', 'cinco', 'seis', 'siete', 'ocho', 'nueve'])
        self.two_syllables_words = set(['cero', 'uno', 'cuatro', 'cinco', 'siete', 'ocho', 'nueve', 'otra'])

        # Load cleaning model if required.
        if args.cleaner_model_path != '':
            self.cleaner_model = torch.load(args.cleaner_model_path)


    def check_folder_structure(self):
        """Checks that PulseID folder exists, and that lab and ppg folders are present as well."""
        if not os.path.isdir(self.pulseID_pathdir):
            raise ValueError('Pulse ID directory has not been found! Please set a correct path to it.')
        else:
            if not os.path.isdir(self.pulseID_ppg_pathdir):
                raise ValueError('Pulse ID PPG files directory has not been found! Please set a correct path to it.')
            elif not os.path.isdir(self.pulseID_lab_pathdir):
                raise ValueError('Pulse ID lab files directory has not been found! Please set a correct path to it.')

    def store_filepaths_in_set(self, pathdir, file_type):
        """Stores all the file paths under a given directory in a set."""
        storage_set = set()
        directory = os.fsencode(pathdir)

        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            if filename.endswith(file_type):
                filepath = os.path.join(pathdir, filename)
                storage_set.add(filepath)
                continue
            else:
                continue

        return storage_set

    def store_lab_filepaths(self):
        """Wrapper for storing the paths to the PulseID lab files in a set."""
        return self.store_filepaths_in_set(self.pulseID_lab_pathdir, file_type=".lab")

    def store_ppg_filepaths(self):
        """Wrapper for storing the paths to the PulseID ppg files in a set."""
        return self.store_filepaths_in_set(self.pulseID_ppg_pathdir, file_type=".txt")

    def convert_lab_to_ppg_name(self, lab_filepath):
        """Convert the a lab's filepath to its corresponding ppg name."""
        ppg_path = lab_filepath.replace("/lab/", "/ppg/")
        ppg_path = ppg_path.replace("Voice", "Pulse")
        ppg_path = ppg_path.replace(".lab", ".txt")
        return ppg_path

    def convert_ppg_to_lab_name(self, ppg_filepath):
        """Convert the a ppg's filepath to its corresponding lab name."""
        lab_path = ppg_filepath.replace("/ppg/", "/lab/")
        lab_path = lab_path.replace("Pulse", "Voice")
        lab_path = lab_path.replace(".txt", ".lab")
        return lab_path

    def create_lab_ppg_dictionary(self):
        """Every lab file should have a related ppg file, so link these two in a dictionary."""
        lab_ppg_dict = {}
        for lab_filepath in self.lab_filepaths:
            related_ppg_path = self.convert_lab_to_ppg_name(lab_filepath)
            if not os.path.isfile(related_ppg_path):
                raise ValueError('Corresponding ppg file to the current lab file has not been found: ' + str(related_ppg_path))
            else:
                lab_ppg_dict[lab_filepath] = related_ppg_path

        return lab_ppg_dict

    def compute_mean_word_time(self, dataframe, experiment_ids, discard_silences=True, include_catalan=False, include_noisy_wavs=False):
        """Computes how long does every word last, and returns the mean and standard deviation of the duration of all words."""
        word_times = []
        for index, row in dataframe.iterrows():
            # If for some reason the sample has no values, discard it.
            if len(row[self.PULSEID_SAMPLE_COLUMN]) == 0:
                continue

            # Discard all the samples that are not in the desired experiment IDs.
            if int(row[self.PULSEID_EXPERIMENT_ID_COLUMN]) not in experiment_ids:
                continue

            # If the sample is in catalan, and it is not desired to include it, discard it.
            if (include_catalan != True) and (row[self.PULSE_ID_LANGUAGE_COLUMN] == "Catalan"):
                continue

            # If the sample is noisy, and it is not desired to include it, discard it.
            if (include_noisy_wavs != True) and (row[self.PULSE_ID_AUDIO_STATUS] == "Noisy"):
                continue

            if discard_silences and row[self.PULSEID_LABEL_COLUMN] == "Non-speech":
                continue

            word_times.append(row[self.PULSE_ID_DURATION])
        return np.mean(word_times), np.std(word_times), np.min(word_times), np.max(word_times)

    def get_audio_status(self, subject_id):
        if subject_id in self.noisy_wavs_ids:
            return "Noisy"
        else:
            return "Clean"

    def get_gender(self, subject_id):
        if subject_id in self.female_speaker_ids:
            return "Female"
        else:
            return "Male"

    def get_language(self, subject_id, experiment_id):
        if (subject_id in self.catalan_speaker_ids) and (experiment_id in self.language_sensitive_experiment_ids):
            return"Catalan"
        else:
            return "Spanish"

    def get_subject_id(self, filepath):
        """Get the Subject ID from a lab or ppg filepath."""
        subject_start_index = filepath.find("S0")
        subject_end_index = filepath.find("_", subject_start_index)
        subject_id = filepath[subject_start_index:subject_end_index]
        return subject_id

    def get_session_id(self, filepath):
        """Get the Session ID from a lab or ppg filepath."""
        session_start_index = filepath.find("SS")
        session_end_index = filepath.find("_", session_start_index)
        session_id = filepath[session_start_index:session_end_index]
        return session_id

    def get_experiment_id(self, filepath):
        """Get the Experiment ID from a lab or ppg filepath."""
        experiment_end_index = filepath.find(".lab")
        if experiment_end_index == -1:
            experiment_end_index = filepath.find(".txt")
        experiment_start_index = experiment_end_index - 1
        experiment_id = filepath[experiment_start_index:experiment_end_index]
        return experiment_id

    def get_words_from_interval(self, lab_filepath, start_timestamp, end_timestamp):
        """Returns a list with the words annotated in a lab file in a certain time interval, also the percentage of speech in the interval."""
        lab_data = pd.read_csv(lab_filepath, sep=self.LAB_SEPARATOR, header=None)
        lab_data.columns = [self.LAB_INITIAL_TIME_COLUMN, self.LAB_FINAL_TIME_COLUMN, self.LAB_LABEL_COLUMN]

        words = []
        total_time = end_timestamp - start_timestamp
        speech_time = 0
        for index, row in lab_data.iterrows():
            if (row[self.LAB_INITIAL_TIME_COLUMN] <= start_timestamp <= row[self.LAB_FINAL_TIME_COLUMN]) or (start_timestamp <= row[self.LAB_INITIAL_TIME_COLUMN] <= end_timestamp):
               words.append(row[self.LAB_LABEL_COLUMN])

            if (row[self.LAB_INITIAL_TIME_COLUMN] <= start_timestamp <= row[self.LAB_FINAL_TIME_COLUMN]):
                speech_time += row[self.LAB_FINAL_TIME_COLUMN] - start_timestamp

            if (start_timestamp <= row[self.LAB_INITIAL_TIME_COLUMN] <= end_timestamp):
                speech_time += end_timestamp - row[self.LAB_INITIAL_TIME_COLUMN]

            # In order to save time, do not search for labels after the end timestamp.
            if row[self.LAB_INITIAL_TIME_COLUMN] > end_timestamp:
                break

        speech_percentage = (speech_time/total_time)*100
        return words, speech_percentage

    def extract_experiment_info(self, filepath):
        """Wrapper for getting the Subject, Session and Experiment IDs from a lab or ppg filepath."""
        return self.get_subject_id(filepath), self.get_session_id(filepath), self.get_experiment_id(filepath)
    
    def extract_info_ppg(self, filepath):
        subject_id, session_id, experiment_id = self.extract_experiment_info(filepath)
        language = self.get_language(subject_id, experiment_id)
        audio_status = self.get_audio_status(subject_id)
        gender = self.get_gender(subject_id)

        return subject_id, session_id, experiment_id, language, audio_status, gender

    def filter_sample(self, args, experiment_ids, filepath, subject_id, session_id, experiment_id, language, audio_status, gender):
        # Discard samples from unclear files.
        if (experiment_id not in self.experiment_id_whitelist) or (session_id not in self.session_id_whitelist) or (subject_id in self.subject_id_blacklist):
            return True

        # For speech experiments check that the corresponding lab file exists, skip it otherwise.
        if (experiment_id != self.EXPERIMENT_SILENCE):
            lab_filepath = self.convert_ppg_to_lab_name(filepath)
            if not os.path.isfile(lab_filepath):
                return True

        # Discard all the samples that are not in the desired experiment IDs.
        if int(experiment_id) not in experiment_ids:
            return True

        # Discard all the samples that are not in the desired subject IDs.
        if (args.subject_id != 'all') and (subject_id != args.subject_id):
            return True

        # If the sample is in catalan, and it is not desired to include it, discard it.
        if (args.include_catalan != True) and (language == "Catalan"):
            return True

        # If the sample is noisy, and it is not desired to include it, discard it.
        if (args.include_noisy_wavs != True) and (audio_status == "Noisy"):
            return True

        return False

    def butter_bandpass(self, lowcut, highcut, fs, order=5):
        nyq = 0.5 * fs
        low = lowcut / nyq
        high = highcut / nyq
        b, a = signal.butter(order, [low, high], btype='band')
        return b, a

    def butter_bandpass_filter(self, data, lowcut, highcut, fs, order=5):
        b, a = self.butter_bandpass(lowcut, highcut, fs, order=order)
        y = signal.lfilter(b, a, data)
        return y

    def clean_signal(self, args, sample):
        """Clean the signal with the preloaded Cleaner Model."""
        # Adapt the sample to the format and data type (tensor) required by the model.
        sample_size = int(args.time_window*self.SAMPLES_PER_SECOND)
        sample = torch.from_numpy(sample)
        sample = sample.expand(1, sample_size)

        cleaned_signal = self.cleaner_model(sample)[0][0].detach().numpy()
        return cleaned_signal

    def process_signal(self, args, sample):
        # Clean the sample with the Cleaner Model, if required.
        if args.cleaner_model_path:
            sample = self.clean_signal(args, sample)
        # Apply hamming window to the sample if required.
        if args.window_type == 'hamming':
            sample = sample*np.hamming(len(sample))

        return sample

    def get_ppg_from_file(self, args, filepath):
        ppg_data = pd.read_csv(filepath, sep=self.PPG_SEPARATOR, header=None)
        ppg_data.columns = [self.PPG_TIME_COLUMN, self.PPG_VOLTAGE_COLUMN]
        time = ppg_data[self.PPG_TIME_COLUMN].values
        ppg_signal = ppg_data[self.PPG_VOLTAGE_COLUMN].values

        if args.normalize:
            ppg_signal = self.z_score_normalization(ppg_signal)

        if args.apply_gaussian_filter1D:
            ppg_signal = ndimage.gaussian_filter1d(ppg_signal, sigma=1, order=0)

        #ppg_signal = self.butter_bandpass_filter(ppg_signal, 2, 50, 200)
        return ppg_signal, time

    def get_prosody_position(self, args, subject_id, session_id, experiment_id, word_number, lab_dataframe):
        """Return the prosody position of a number or word within a phrase (Begin (B), Inside (I), Last (L))"""
        if experiment_id == 1:
            return self.get_prosody_position_expID_1(args, word_number)
        elif experiment_id == 3:
            return self.get_prosody_position_expID_3(args, word_number)
        elif experiment_id == 4:
            return self.get_prosody_position_expID_4(args, word_number, lab_dataframe)

    def get_prosody_position_expID_1(self, args, word_number):
        """For experiment 1, two card numbers are said, each one contains 16 digits.
        If BIL tagging is used, 1-5=B, 6-12=I, 13-15=L.
        If BL tagging is used, 1-12=B, 13-15=L."""
        if word_number > 16:
            word_number -= 16
        if args.prosody_position_tag == 'BIL':
            if (1 <= word_number <= 5):
                return 'B'
            elif (6 <= word_number <= 12):
                return 'I'
            elif (13 <= word_number <= 16):
                return 'L'
        elif args.prosody_position_tag == 'BL':
            if (1 <= word_number <= 12):
                return 'B'
            elif (13 <= word_number <= 16):
                return 'L'

        return 'Unknown'

    def get_prosody_position_expID_3(self, args, word_number):
        """For experiment 3, four PIN numbers are said, each one containing 6 digits.
        If BIL tagging is used, 1-2=B, 3-4=I, 5-6=L.
        If BL tagging is used, 1-4=B, 5-6=L."""
        if (7 <= word_number <= 12):
            word_number -= 6
        elif (13 <= word_number <= 18):
            word_number -= 12
        elif (19 <= word_number <= 24):
            word_number -= 18

        if args.prosody_position_tag == 'BIL':
            if (1 <= word_number <= 2):
                return 'B'
            elif (3 <= word_number <= 4):
                return 'I'
            elif (5 <= word_number <= 6):
                return 'L'
        elif args.prosody_position_tag == 'BL':
            if (1 <= word_number <= 4):
                return 'B'
            elif (5 <= word_number <= 6):
                return 'L'

        return 'Unknown'

    def get_prosody_position_expID_4(self, args, word_number, lab_dataframe):
        """For experiment 4, two sentences are said, each one containing different number of words."""
        word_index = 1

        # First, try to find where every sentence begins or ends.
        previous_word_end = 0
        wordindex_tag_dict = {}
        for index, row in lab_dataframe.iterrows():
            word_initial_time = row[self.LAB_INITIAL_TIME_COLUMN]
            word_final_time = row[self.LAB_FINAL_TIME_COLUMN]

            if word_initial_time - previous_word_end > 0.8:
                wordindex_tag_dict[word_index] = 'B'
                wordindex_tag_dict[word_index - 1] = 'L'
            else:
                wordindex_tag_dict[word_index] = 'B'

            previous_word_end = word_final_time
            word_index += 1

        # The last word said shall be tagged as L.
        wordindex_tag_dict[word_index - 1] = 'L'

        if args.prosody_position_tag == 'BIL':
            for key, value in wordindex_tag_dict.items():
                if key == 1:
                    continue
                else:
                    if (value == 'B') and (wordindex_tag_dict[key - 1] != 'L'):
                        wordindex_tag_dict[key] = 'I'

        return wordindex_tag_dict[word_number]


    def adjust_to_time_window(self, args, signal, time):
        signal_length = len(signal)
        sample_size = int(self.SAMPLES_PER_SECOND*args.time_window)

        signals = []
        times = []
        if args.include_padded_samples and signal_length < sample_size:
            signals = [self.zero_pad_sample(signal, sample_size)]
            times.append(time)
        elif not args.include_padded_samples and signal_length < sample_size:
            signals = []
        
        if signal_length > sample_size:
            start_position = 0
            end_position = sample_size
            # Get the signal windows as required by sample size and stride arguments.
            while end_position <= len(signal):
                signals.append(signal[start_position:end_position])
                times.append(time[start_position:end_position])
                start_position += int((1 - args.overlap)*sample_size)
                end_position += int((1 - args.overlap)*sample_size)

        # Apply hamming window to the samples if required.
        if args.window_type == 'hamming':
            for index, sub_signal in enumerate(signals):
                signals[index] = sub_signal*np.hamming(len(sub_signal))

        return signals, times

    def speech_pruning(self, args, dataframe):
        """Takes an experiment's PPG signal and extracts the signal from every speech and non-speech event."""
        sample_size = int(self.SAMPLES_PER_SECOND*args.time_window)
        sample_ID = 0

        dataframe_list = []
        for index, row in dataframe.iterrows():
            # Extract the sample's information from the dataframe.
            ppg_signal = row[self.PULSEID_SAMPLE_COLUMN]
            time = row[self.PULSEID_TIME_COLUMN]
            ppg_filepath = row[self.PULSEID_FILEPATH_COLUMN]
            experiment_id = int(row[self.PULSEID_EXPERIMENT_ID_COLUMN])
            subject_id = row[self.PULSEID_SUBJECT_ID_COLUMN]
            session_id = row[self.PULSEID_SESSION_ID_COLUMN]
            audio_status = row[self.PULSE_ID_AUDIO_STATUS]
            gender = row[self.PULSE_ID_GENDER]
            language = row[self.PULSE_ID_LANGUAGE_COLUMN]
            sample_length = len(ppg_signal)
            duration = args.time_window

            if experiment_id != int(self.EXPERIMENT_SILENCE):
                lab_filepath = self.convert_ppg_to_lab_name(ppg_filepath)
                lab_data = pd.read_csv(lab_filepath, sep=self.LAB_SEPARATOR, header=None)
                lab_data.columns = [self.LAB_INITIAL_TIME_COLUMN, self.LAB_FINAL_TIME_COLUMN, self.LAB_LABEL_COLUMN]

                nonspeech_start_position = 0
                word_number = 1
                # Loop through lab file data, to get the initial and final time of a speech event.
                for index, row in lab_data.iterrows():
                    nonspeech_end_position = int(row[self.LAB_INITIAL_TIME_COLUMN]*self.SAMPLES_PER_SECOND) - 2
                    speech_start_position = nonspeech_end_position
                    speech_end_position = int(row[self.LAB_FINAL_TIME_COLUMN]*self.SAMPLES_PER_SECOND)

                    if not args.discard_silences:
                        nonspeech_signal = ppg_signal[nonspeech_start_position:nonspeech_end_position]
                        nonspeech_time = time[nonspeech_start_position:nonspeech_end_position]
                        nonspeech_subsignals, nonspeech_subtimes = self.adjust_to_time_window(args, nonspeech_signal, nonspeech_time)
                        if nonspeech_subsignals != []:
                            for i in range(len(nonspeech_subsignals)):
                                dataframe_list.append([nonspeech_subsignals[i], self.NONSPEECH_LABEL, ['<None>'], nonspeech_subtimes[i], ppg_filepath, sample_ID, subject_id, session_id, experiment_id, language, audio_status, gender, 0, duration, 'None'])
                        sample_ID += 1

                    speech_signal = ppg_signal[speech_start_position:speech_end_position]
                    speech_time = time[speech_start_position:speech_end_position]
                    speech_label = row[self.LAB_LABEL_COLUMN]
                    speech_words = [speech_label]
                    speech_prosody_position = self.get_prosody_position(args, subject_id, session_id, experiment_id, word_number, lab_data)
                    speech_subsignals, speech_subtimes = self.adjust_to_time_window(args, speech_signal, speech_time)
                    if speech_subsignals != []:
                        for i in range(len(speech_subsignals)):
                            dataframe_list.append([speech_subsignals[i], speech_label, speech_words, speech_subtimes[i], ppg_filepath, sample_ID, subject_id, session_id, experiment_id, language, audio_status, gender, self.get_syllables_count(speech_label, language), duration, speech_prosody_position])
                    sample_ID += 1
                    nonspeech_start_position = speech_end_position + 1

                    # For final iteration, save all the remaining non-speech pulse.
                    if not args.discard_silences:
                        if index == len(lab_data.index) - 1:
                            nonspeech_signal = ppg_signal[nonspeech_start_position:]
                            nonspeech_time = time[nonspeech_start_position:]
                            nonspeech_subsignals, nonspeech_subtimes = self.adjust_to_time_window(args, nonspeech_signal, nonspeech_time)
                            if nonspeech_subsignals != []:
                                for i in range(len(nonspeech_subsignals)):
                                    dataframe_list.append([nonspeech_subsignals[i], self.NONSPEECH_LABEL, ['<None>'], nonspeech_subtimes[i], ppg_filepath, sample_ID, subject_id, session_id, experiment_id, language, audio_status, gender, 0, duration, 'None'])
                    word_number += 1

            else:
                nonspeech_subsignals, nonspeech_subtimes = self.adjust_to_time_window(args, ppg_signal, time)
                if nonspeech_subsignals != []:
                    for i in range(len(nonspeech_subsignals)):
                        dataframe_list.append([nonspeech_subsignals[i], self.NONSPEECH_LABEL, ['<None>'], nonspeech_subtimes[i], ppg_filepath, sample_ID, subject_id, session_id, experiment_id, language, audio_status, gender, 0, duration, 'None'])
                sample_ID += 1

        return pd.DataFrame(dataframe_list, columns=[self.PULSEID_SAMPLE_COLUMN, self.PULSEID_LABEL_COLUMN, self.PULSEID_WORDS_COLUMN, self.PULSEID_TIME_COLUMN, self.PULSEID_FILEPATH_COLUMN, self.PULSEID_SAMPLE_ID_COLUMN,
                                                     self.PULSEID_SUBJECT_ID_COLUMN, self.PULSEID_SESSION_ID_COLUMN, 
                                                     self.PULSEID_EXPERIMENT_ID_COLUMN, self.PULSE_ID_LANGUAGE_COLUMN,
                                                     self.PULSE_ID_AUDIO_STATUS, self.PULSE_ID_GENDER, self.PULSE_ID_SYLLABLES_COUNT, self.PULSE_ID_DURATION, self.PULSE_ID_PROSODY_POSITION])

    def speech_recognition(self, args, dataframe):
        """Takes an experiment's PPG signal and parses the signal using a window."""
        sample_size = int(self.SAMPLES_PER_SECOND*args.time_window)
        sample_stride = int(self.SAMPLES_PER_SECOND*args.sampling_stride)
        sample_ID = 0

        dataframe_list = []
        for index, row in dataframe.iterrows():
            # Extract the sample's information from the dataframe.
            ppg_signal = row[self.PULSEID_SAMPLE_COLUMN]
            time = row[self.PULSEID_TIME_COLUMN]
            ppg_filepath = row[self.PULSEID_FILEPATH_COLUMN]
            experiment_id = int(row[self.PULSEID_EXPERIMENT_ID_COLUMN])
            subject_id = row[self.PULSEID_SUBJECT_ID_COLUMN]
            session_id = row[self.PULSEID_SESSION_ID_COLUMN]
            audio_status = row[self.PULSE_ID_AUDIO_STATUS]
            gender = row[self.PULSE_ID_GENDER]
            language = row[self.PULSE_ID_LANGUAGE_COLUMN]
            sample_length = len(ppg_signal)
            duration = args.time_window
            syllables_count = 0

            if experiment_id != int(self.EXPERIMENT_SILENCE):
                lab_filepath = self.convert_ppg_to_lab_name(ppg_filepath)
                lab_data = pd.read_csv(lab_filepath, sep=self.LAB_SEPARATOR, header=None)
                lab_data.columns = [self.LAB_INITIAL_TIME_COLUMN, self.LAB_FINAL_TIME_COLUMN, self.LAB_LABEL_COLUMN]

                first_speech_time = lab_data.iloc[0][self.LAB_INITIAL_TIME_COLUMN]
                last_speech_time = lab_data.iloc[-1][self.LAB_FINAL_TIME_COLUMN]

                # Get the speech samples, between the first moment that a speech event occurs and the last one.
                start_position = int(first_speech_time*self.SAMPLES_PER_SECOND)
                end_position = int(start_position + sample_size)

                # Get the signal windows as required by sample size and stride arguments.
                while end_position <= last_speech_time*self.SAMPLES_PER_SECOND:
                    words = []
                    start_timestamp = start_position/self.SAMPLES_PER_SECOND
                    end_timestamp = end_position/self.SAMPLES_PER_SECOND

                    words, speech_percentage = self.get_words_from_interval(lab_filepath, start_timestamp, end_timestamp)
                    if not words or speech_percentage < args.min_speech_percentage:
                        start_position += int((1 - args.overlap)*sample_size)
                        end_position += int((1 - args.overlap)*sample_size)
                        continue
                    else:
                        label = 'Speech'

                    # Extract the sample.
                    sample = ppg_signal[start_position:end_position]
                    sample = self.process_signal(args, sample)

                    dataframe_list.append([sample, label, words, subject_id, session_id, experiment_id, language, audio_status, gender, syllables_count, duration])
                    start_position += int((1 - args.overlap)*sample_size)
                    end_position += int((1 - args.overlap)*sample_size)

                # Get the non-speech samples, after the last moment where a speech event occurs.
                start_position = int(last_speech_time*self.SAMPLES_PER_SECOND)
                end_position = int(start_position + sample_size)
                # Get the signal windows as required by sample size and stride arguments.
                while end_position <= len(ppg_signal):
                    label = 'Non-speech'
                    words = ['<None>']

                    # Extract the sample.
                    sample = ppg_signal[start_position:end_position]
                    sample = self.process_signal(args, sample)

                    dataframe_list.append([sample, label, words, subject_id, session_id, experiment_id, language, audio_status, gender, syllables_count, duration])
                    start_position += int((1 - args.overlap)*sample_size)
                    end_position += int((1 - args.overlap)*sample_size)

            else:
                # Get the non-speech samples from the silence experiment.
                start_position = 0
                end_position = int(start_position + sample_size)
                # Get the signal windows as required by sample size and stride arguments.
                while end_position <= len(ppg_signal):
                    words = []
                    start_timestamp = start_position/self.SAMPLES_PER_SECOND
                    end_timestamp = end_position/self.SAMPLES_PER_SECOND
                    label = 'Non-speech'
                    words = ['<None>']

                    # Extract the sample.
                    sample = ppg_signal[start_position:end_position]
                    sample = self.process_signal(args, sample)

                    dataframe_list.append([sample, label, words, subject_id, session_id, experiment_id, language, audio_status, gender, syllables_count, duration])
                    start_position += int((1 - args.overlap)*sample_size)
                    end_position += int((1 - args.overlap)*sample_size)

        return pd.DataFrame(dataframe_list, columns=[self.PULSEID_SAMPLE_COLUMN, self.PULSEID_LABEL_COLUMN, self.PULSEID_WORDS_COLUMN, 
                                                     self.PULSEID_SUBJECT_ID_COLUMN, self.PULSEID_SESSION_ID_COLUMN, 
                                                     self.PULSEID_EXPERIMENT_ID_COLUMN, self.PULSE_ID_LANGUAGE_COLUMN,
                                                     self.PULSE_ID_AUDIO_STATUS, self.PULSE_ID_GENDER, self.PULSE_ID_SYLLABLES_COUNT, self.PULSE_ID_DURATION])

    def generate_pulseID_dataframe(self, args, experiment_ids):
        """Generates a dataframe containing all the information in Pulse ID data set."""
        dataframe_list = []

        for ppg_filepath in self.ppg_filepaths:
            # Retrieve information about the PPG recording.
            subject_id, session_id, experiment_id, language, audio_status, gender = self.extract_info_ppg(ppg_filepath)

            # Filter the sample according to the execution arguments.
            if self.filter_sample(args, experiment_ids, ppg_filepath, subject_id, session_id, experiment_id, language, audio_status, gender):
                continue

            # Get the PPG signal and the time stamps in the recording.
            ppg_signal, time = self.get_ppg_from_file(args, ppg_filepath)

            # Further useful information. For the moment, do not put any label nor any word nor syllable count to the samples.
            label = 'None'
            words = ['<None>']
            syllables_count = 0
            duration = float(len(ppg_signal)/self.SAMPLES_PER_SECOND)

            dataframe_list.append([ppg_signal, label, words, time, ppg_filepath, subject_id, session_id, experiment_id, language, audio_status, gender, syllables_count, duration])

        return pd.DataFrame(dataframe_list, columns=[self.PULSEID_SAMPLE_COLUMN, self.PULSEID_LABEL_COLUMN, self.PULSEID_WORDS_COLUMN, self.PULSEID_TIME_COLUMN, self.PULSEID_FILEPATH_COLUMN,
                                                     self.PULSEID_SUBJECT_ID_COLUMN, self.PULSEID_SESSION_ID_COLUMN, 
                                                     self.PULSEID_EXPERIMENT_ID_COLUMN, self.PULSE_ID_LANGUAGE_COLUMN,
                                                     self.PULSE_ID_AUDIO_STATUS, self.PULSE_ID_GENDER, self.PULSE_ID_SYLLABLES_COUNT, self.PULSE_ID_DURATION])

    def process_pulseID_dataframe(self, args, dataframe, experiment_ids):
        """Process the Pulse ID signals with the desired method, so they are subsampled and labeled."""
        # Preprocess the data.
        if args.data_processing == 'speech_pruning':
            dataframe = self.speech_pruning(args, dataframe)
        elif args.data_processing == 'speech_recognition':
            dataframe = self.speech_recognition(args, dataframe)

        return dataframe

    # TO BE DEPRECATED
    def generate_pulseID_dataframe_old(self, args):
        """Generates a dataframe containing all the information in Pulse ID data set."""
        
        # Generate a dataframe with samples labeled as speech or non-speech.
        if args.speech_recognition:
            return self.generate_speech_recognition_df(args)

        dataframe_list = []

        for lab_filepath, ppg_filepath in self.lab_ppg_dict.items():
            subject_id, session_id, experiment_id = self.extract_experiment_info(lab_filepath)

            language = self.get_language(subject_id, experiment_id)
            audio_status = self.get_audio_status(subject_id)
            gender = self.get_gender(subject_id)

            lab_data = pd.read_csv(lab_filepath, sep=self.LAB_SEPARATOR, header=None)
            lab_data.columns = [self.LAB_INITIAL_TIME_COLUMN, self.LAB_FINAL_TIME_COLUMN, self.LAB_LABEL_COLUMN]

            ppg_data = pd.read_csv(ppg_filepath, sep=self.PPG_SEPARATOR, header=None)
            ppg_data.columns = [self.PPG_TIME_COLUMN, self.PPG_VOLTAGE_COLUMN]

            last_final_time = 0
            voice_data_row_number = len(lab_data.index)

            # Loop through lab file data, to get the initial and final time of a speech event.
            for index, row in lab_data.iterrows():
                initial_time = row[self.LAB_INITIAL_TIME_COLUMN]
                final_time = row[self.LAB_FINAL_TIME_COLUMN]
                label = row[self.LAB_LABEL_COLUMN]
                words = [label]
                syllables_count = self.get_syllables_count(label, language)

                # Now loop through PPG Data, and store the data between initial and final time in a numpy array.
                speech_data = ppg_data[(ppg_data[self.PPG_TIME_COLUMN] >= initial_time) & (ppg_data[self.PPG_TIME_COLUMN] <= final_time)]
                speech_sample = speech_data[self.PPG_VOLTAGE_COLUMN].values

                # Save non-speech data in a separate numpy array.
                nonspeech_data = ppg_data[(ppg_data[self.PPG_TIME_COLUMN] >= last_final_time) & (ppg_data[self.PPG_TIME_COLUMN] <= initial_time)]
                nonspeech_sample = nonspeech_data[self.PPG_VOLTAGE_COLUMN].values

                last_final_time = final_time

                # For final iteration, save all the remaining non-speech pulse.
                if index == voice_data_row_number - 1:
                    nonspeech_data = ppg_data[(ppg_data[self.PPG_TIME_COLUMN] >= last_final_time)]

                speech_duration = len(speech_sample)/self.SAMPLES_PER_SECOND
                nonspeech_duration = len(nonspeech_sample)/self.SAMPLES_PER_SECOND

                dataframe_list.append([speech_sample, label, words, subject_id, session_id, experiment_id, language, audio_status, gender, syllables_count, speech_duration])
                dataframe_list.append([nonspeech_sample, self.NONSPEECH_LABEL, ['<None>'], subject_id, session_id, experiment_id, language, audio_status, gender, 0, nonspeech_duration])

        # Additional loop for storing experiment 2 (silence) information.
        for ppg_filepath in self.ppg_filepaths:
            subject_id, session_id, experiment_id = self.extract_experiment_info(ppg_filepath)

            if experiment_id != self.EXPERIMENT_SILENCE:
                continue
            # Discard samples from unclear files.
            if (experiment_id not in self.experiment_id_whitelist) or (session_id not in self.session_id_whitelist) or (subject_id in self.subject_id_blacklist):
                continue

            ppg_data = pd.read_csv(ppg_filepath, sep=self.PPG_SEPARATOR, header=None)
            ppg_data.columns = [self.PPG_TIME_COLUMN, self.PPG_VOLTAGE_COLUMN]
            ppg_signal = ppg_data[self.PPG_VOLTAGE_COLUMN].values

            language = self.get_language(subject_id, experiment_id)
            audio_status = self.get_audio_status(subject_id)
            gender = self.get_gender(subject_id)
            syllables_count = 0
            nonspeech_duration = len(ppg_signal)/self.SAMPLES_PER_SECOND

            dataframe_list.append([ppg_signal, self.NONSPEECH_LABEL, ['<None>'], subject_id, session_id, experiment_id, language, audio_status, gender, 0, nonspeech_duration])

        return pd.DataFrame(dataframe_list, columns=[self.PULSEID_SAMPLE_COLUMN, self.PULSEID_LABEL_COLUMN, self.PULSEID_WORDS_COLUMN,
                                                     self.PULSEID_SUBJECT_ID_COLUMN, self.PULSEID_SESSION_ID_COLUMN, 
                                                     self.PULSEID_EXPERIMENT_ID_COLUMN, self.PULSE_ID_LANGUAGE_COLUMN,
                                                     self.PULSE_ID_AUDIO_STATUS, self.PULSE_ID_GENDER, self.PULSE_ID_SYLLABLES_COUNT, self.PULSE_ID_DURATION])

    def generate_speech_recognition_df(self, args):
        """Generates a dataframe containing all the information in Pulse ID data set, for speech recognition tasks."""
        dataframe_list = []
        sample_size = int(self.SAMPLES_PER_SECOND*args.time_window)
        sample_stride = int(self.SAMPLES_PER_SECOND*args.sampling_stride)

        for ppg_filepath in self.ppg_filepaths:
            subject_id, session_id, experiment_id = self.extract_experiment_info(ppg_filepath)
            # Discard samples from unclear files.
            if (experiment_id not in self.experiment_id_whitelist) or (session_id not in self.session_id_whitelist) or (subject_id in self.subject_id_blacklist):
                continue

            # For speech experiments check that the corresponding lab file exists, skip it otherwise.
            if (experiment_id != self.EXPERIMENT_SILENCE):
                lab_filepath = self.convert_ppg_to_lab_name(ppg_filepath)
                if not os.path.isfile(lab_filepath):
                    continue

            language = self.get_language(subject_id, experiment_id)
            audio_status = self.get_audio_status(subject_id)
            gender = self.get_gender(subject_id)
            syllables_count = 0
            speech_duration = args.time_window

            ppg_data = pd.read_csv(ppg_filepath, sep=self.PPG_SEPARATOR, header=None)
            ppg_data.columns = [self.PPG_TIME_COLUMN, self.PPG_VOLTAGE_COLUMN]
            ppg_signal = ppg_data[self.PPG_VOLTAGE_COLUMN].values

            start_position = 0
            end_position = sample_size
            # Get the signal windows as required by sample size and stride arguments.
            while end_position <= len(ppg_signal):
                words = []
                start_timestamp = start_position/self.SAMPLES_PER_SECOND
                end_timestamp = end_position/self.SAMPLES_PER_SECOND
                if (experiment_id != self.EXPERIMENT_SILENCE):
                    words = self.get_words_from_interval(lab_filepath, start_timestamp, end_timestamp)
                if not words:
                    label = 'Non-speech'
                    words = ['<None>']
                else:
                    label = 'Speech'

                dataframe_list.append([ppg_signal[start_position:end_position], label, words, subject_id, session_id, experiment_id, language, audio_status, gender, syllables_count, speech_duration])
                start_position += sample_stride
                end_position += sample_stride

        return pd.DataFrame(dataframe_list, columns=[self.PULSEID_SAMPLE_COLUMN, self.PULSEID_LABEL_COLUMN, self.PULSEID_WORDS_COLUMN,
                                                     self.PULSEID_SUBJECT_ID_COLUMN, self.PULSEID_SESSION_ID_COLUMN, 
                                                     self.PULSEID_EXPERIMENT_ID_COLUMN, self.PULSE_ID_LANGUAGE_COLUMN,
                                                     self.PULSE_ID_AUDIO_STATUS, self.PULSE_ID_GENDER, self.PULSE_ID_SYLLABLES_COUNT, self.PULSE_ID_DURATION])

    def adjust_dataframe(self, args, dataframe, experiment_ids):
        """Adjust the stored Pulse ID dataframe to desired parameters, like time window of samples or margin before
        or after last speech event."""
        sample_size = int(self.SAMPLES_PER_SECOND*args.time_window)

        dataframe_list = []
        for index, row in dataframe.iterrows():
            sample = row[self.PULSEID_SAMPLE_COLUMN]
            label = row[self.PULSEID_LABEL_COLUMN]
            words = row[self.PULSEID_WORDS_COLUMN]
            experiment_id = int(row[self.PULSEID_EXPERIMENT_ID_COLUMN])
            subject_id = row[self.PULSEID_SUBJECT_ID_COLUMN]
            sample_length = len(sample)
            language = row[self.PULSE_ID_LANGUAGE_COLUMN]
            audio_status = row[self.PULSE_ID_AUDIO_STATUS]
            duration = row[self.PULSE_ID_DURATION]

            # Discard all the samples that are not in the desired experiment IDs.
            if experiment_id not in experiment_ids:
                continue

            # Discard all the samples that are not in the desired subject IDs.
            if (args.subject_id != 'all') and (subject_id != args.subject_id):
                continue

            # If for some reason the sample has no values, discard it.
            if sample_length == 0:
                continue

            # If padded sample are not allowed, and the sample is smaller than the time window, discard it.
            if not args.include_padded_samples and (sample_length < sample_size):
                continue

            # If the sample is in catalan, and it is not desired to include it, discard it.
            if (args.include_catalan != True) and (language == "Catalan"):
                continue

            # If the sample is noisy, and it is not desired to include it, discard it.
            if (args.include_noisy_wavs != True) and (audio_status == "Noisy"):
                continue

            if args.discard_silences and label == "Non-speech":
                continue

            if args.discard_speech and label != "Non-speech":
                continue

            # For Speech/Non-speech classification, relabel all speech samples as "Speech"
            if args.only_speech_nonspeech and label != "Non-speech":
                label = 'Speech'

            if args.min_sample_time > duration:
                continue

            # Apply time collar if it is different to 0. This means that the starting and final
            # parts of the sample are removed, in order to take away the influence of speech/non-speech
            # during transitions from one to the other.
            if args.absolute_time_collar != 0:
                sample_collar = int(self.SAMPLES_PER_SECOND*args.absolute_time_collar)

                # The collar cuts the sample at the start and the end. If the cut is actually going to 
                # remove the whole sample, then discard the sample.
                if sample_collar*2 >= sample_length:
                    continue
                else:
                    sample = sample[sample_collar:-sample_collar]
                    sample_length = len(sample)

            # The collar can be applied by percentage of the current sample to be removed.
            # For example, a sample of 60 values can have a 10% (6 samples) removed at the start and the
            # end (3 samples removed at the start, 3 samples removed at the end).
            elif args.relative_time_collar != 0:
                sample_collar = int(sample_length*(args.relative_time_collar/2))
                sample = sample[sample_collar:-sample_collar]
                sample_length = len(sample)

            # # Apply hamming window to the sample if required.
            # if args.window_type == 'hamming':
            #     sample = sample*np.hamming(len(sample))

            # # Apply 1D gaussian filter if required.
            # if args.apply_gaussian_filter1D:
            #     sample = sample - ndimage.gaussian_filter1d(sample, sigma=1, order=0)

            if args.include_padded_samples:
                # If the sample's size is smaller than the window time, then pad it with the previous and following samples, if required.
                if args.contiguous_padding:
                    previous_index = index - 1
                    following_index = index + 1
                    while sample_length < sample_size:
                        padding_length = sample_size - sample_length

                        if padding_length > 1:
                            if dataframe.iloc[previous_index][self.PULSEID_EXPERIMENT_ID_COLUMN] == str(experiment_id):
                                previous_sample = dataframe.iloc[previous_index][self.PULSEID_SAMPLE_COLUMN]
                                sample = np.concatenate((previous_sample[-int(padding_length/2):], sample), axis=0)

                            if len(dataframe.index) > following_index:
                                if dataframe.iloc[following_index][self.PULSEID_EXPERIMENT_ID_COLUMN] == str(experiment_id):
                                    following_sample = dataframe.iloc[following_index][self.PULSEID_SAMPLE_COLUMN]
                                    sample = np.concatenate((sample, following_sample[0:int(padding_length/2)]), axis=0)
                        else:
                            break

                        sample_length = len(sample)
                        previous_index -= 1
                        following_index += 1

                # If the sample is still not full, pad it with zeros.
                if sample_length < sample_size:
                    sample = self.zero_pad_sample(sample, sample_size)

            subsamples = []
            # If the sample's size is bigger than the window time, split it in different subsamples, with the allowed overlap percentage.
            if sample_length > sample_size:
                start_position = 0
                end_position = sample_size
                # Get the signal windows as required by sample size and stride arguments.
                while end_position <= len(sample):
                    subsamples.append(sample[start_position:end_position])

            if args.normalize:
                sample = self.z_score_normalization(sample)

            # Apply hamming window to the sample if required.
            if args.window_type == 'hamming':
                sample = sample*np.hamming(len(sample))

            if args.apply_gaussian_filter1D:
                sample = sample - ndimage.gaussian_filter1d(sample, sigma=1, order=0)

            dataframe_list.append([sample, label, words, row[self.PULSEID_SUBJECT_ID_COLUMN], 
                                  row[self.PULSEID_SESSION_ID_COLUMN], row[self.PULSEID_EXPERIMENT_ID_COLUMN],
                                  row[self.PULSE_ID_LANGUAGE_COLUMN], row[self.PULSE_ID_AUDIO_STATUS], row[self.PULSE_ID_GENDER], row[self.PULSE_ID_SYLLABLES_COUNT], row[self.PULSE_ID_DURATION]])

        return pd.DataFrame(dataframe_list, columns=[self.PULSEID_SAMPLE_COLUMN, self.PULSEID_LABEL_COLUMN, self.PULSEID_WORDS_COLUMN,
                                                     self.PULSEID_SUBJECT_ID_COLUMN, self.PULSEID_SESSION_ID_COLUMN, 
                                                     self.PULSEID_EXPERIMENT_ID_COLUMN, self.PULSE_ID_LANGUAGE_COLUMN, 
                                                     self.PULSE_ID_AUDIO_STATUS, self.PULSE_ID_GENDER, self.PULSE_ID_SYLLABLES_COUNT, self.PULSE_ID_DURATION])

    def get_subdataframe(self, dataframe, column_name, value):
        """Get sub-dataframe fulfilling a certain value for a certain property."""
        dataframe_list = []
        for index, row in dataframe.iterrows():
            if row[column_name] == value:
                dataframe_list.append([row[self.PULSEID_SAMPLE_COLUMN], row[self.PULSEID_LABEL_COLUMN], row[self.PULSEID_SUBJECT_ID_COLUMN], 
                                        row[self.PULSEID_SESSION_ID_COLUMN], row[self.PULSEID_EXPERIMENT_ID_COLUMN],
                                        row[self.PULSE_ID_LANGUAGE_COLUMN], row[self.PULSE_ID_AUDIO_STATUS], row[self.PULSE_ID_GENDER], row[self.PULSE_ID_SYLLABLES_COUNT], row[self.PULSE_ID_DURATION]])
                            
        return pd.DataFrame(dataframe_list, columns=[self.PULSEID_SAMPLE_COLUMN, self.PULSEID_LABEL_COLUMN, 
                                                        self.PULSEID_SUBJECT_ID_COLUMN, self.PULSEID_SESSION_ID_COLUMN, 
                                                        self.PULSEID_EXPERIMENT_ID_COLUMN, self.PULSE_ID_LANGUAGE_COLUMN, 
                                                        self.PULSE_ID_AUDIO_STATUS, self.PULSE_ID_GENDER, self.PULSE_ID_SYLLABLES_COUNT, self.PULSE_ID_DURATION])

    def z_score_normalization(self, sample):
        """Compute the z-score normalization for a sample."""
        mean = sample.mean()
        std = sample.std()
        if std != 0:
            sample = (sample - mean)/std
        return sample

    def zero_pad_sample(self, sample, sample_size):
        """Pad a sample with zeroes until it reaches the size of the desired time window."""
        padding_length = sample_size - len(sample)
        padded_sample = np.pad(sample, (0, padding_length), 'constant', constant_values=0)

        return padded_sample

    def get_labeled_samples(self, dataframe, only_speech_nonspeech=False, gender_classification=False, syllables_classification=False, prosody_position=False):
        """Returns a dictionary where the keys are the samples and the values are the labels."""
        labeled_samples = {}
        for index, row in dataframe.iterrows():
            sample_ID = index

            # If gender classification is done as a function of PPG signal, get the labels as male/female.
            if gender_classification:
                labeled_samples[sample_ID] = row[self.PULSE_ID_GENDER]
            # If syllables classification is done as a function of PPG signal, get the labels as the count of syllables.
            elif syllables_classification:
                labeled_samples[sample_ID] = str(row[self.PULSE_ID_SYLLABLES_COUNT])
            # If prosody position classification is done as a function of PPG signal, get the labels as the prosody BIL tag.
            elif prosody_position:
                labeled_samples[sample_ID] = row[self.PULSE_ID_PROSODY_POSITION]
            # Otherwise, use the labels as the word labeled from .lab files.
            else:
                label = row[self.PULSEID_LABEL_COLUMN]
                if only_speech_nonspeech and (label != "Non-speech"):
                    labeled_samples[sample_ID] = "Speech"
                else:
                    labeled_samples[sample_ID] = label

        return labeled_samples

    def get_partition(self, train_dataframe, validation_dataframe, test_dataframe):
        train_ID_list = []
        for index, row in train_dataframe.iterrows():
            train_ID_list.append(index)

        validation_ID_list = []
        for index, row in validation_dataframe.iterrows():
            validation_ID_list.append(index)

        test_ID_list = []
        for index, row in test_dataframe.iterrows():
            test_ID_list.append(index)

        return {'train': train_ID_list, 'validation': validation_ID_list, 'test': test_ID_list}

    def get_syllables_count(self, label, language):
        """Counts the number of syllables in a word."""
        if label == '<oov>':
            return 0

        # For the moment, catalan language is only present in experiment IDs 1 and 3, both only containing numbers.
        # Digits in catalan can only have one or two syllables. 
        if language == 'Catalan':
            if label in self.one_syllable_catalan_words:
                return 1
            else:
                return 2
        else:
            if label in self.one_syllable_words:
                return 1
            elif label in self.two_syllables_words:
                return 2
            # If the label is not known in this class' database, use Pyphen to automatically detect its number
            # of syllables. Pyphen is not 100% accuracy, but still a pretty good approximation.
            else:
                pyphen.language_fallback('es')
                dic = pyphen.Pyphen(lang='es')
                hyphenated_word = dic.inserted(label)
                return (hyphenated_word.count('-') + 1)

    def data_visualization(self, args, dataframe, experiment_ids, logger):
        """Plots all the data visualization graphs."""
        self.plot_sample_number_distribution(args, dataframe, logger)

    def plot_sample_number_distribution(self, args, dataframe, logger):
        """Plots the number of samples per category label."""
        if args.gender_classification:
            category_group = dataframe.groupby([self.PULSE_ID_GENDER]).count()
        elif args.prosody_position:
            category_group = dataframe.groupby([self.PULSE_ID_PROSODY_POSITION]).count()
        else:
            category_group = dataframe.groupby([self.PULSEID_LABEL_COLUMN]).count()
        category_group = category_group[self.PULSEID_SAMPLE_COLUMN]
        plot = category_group.reindex(category_group.sort_values().index).plot(kind='bar', stacked=False, title="Number of Pulse Samples per Category", figsize=(16,10))
        plot.set_xlabel("Category")
        plot.set_ylabel("Number of Samples")
        fig = plot.get_figure()
        fig.savefig(logger.graphs_dir_path + 'SampleNumberDistribution.png')

    def plot_raw_samples(self, args, dataframe, logger):
        """Plots a few raw samples for each label."""
        return 0

