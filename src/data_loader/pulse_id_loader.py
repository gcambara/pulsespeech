# coding=utf-8
# Copyright 2018 jose.fonollosa@upc.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Data generator for the Pulse ID data set."""

import torch
import pickle
import pandas as pd
from torch.utils import data
import torch
import torch.utils.data as data

SYLLABLES_DIGITS_CLASSES = ['0', '1', '2']
SYLLABLES_DIGITS_CLASSES_NO_SILENCE = ['1', '2']
SYLLABLES_CORPUS_CLASSES = ['0', '1', '2', '3', '4', '5', '6']
SYLLABLES_CORPUS_CLASSES_NO_SILENCE = ['1', '2', '3', '4', '5', '6']
GENDER_CLASSES = ['Female', 'Male']
SPEECH_NONSPEECH_CLASSES = ['Non-speech', 'Speech']
PROSODY_POSITION_CLASSES_BIL = ['B', 'I', 'L']
PROSODY_POSITION_CLASSES_BL = ['B', 'L'] 
NUMBERS_CLASSES = ['cero', 'uno', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete', 'ocho', 'nueve', 'Non-speech']
NUMBERS_CLASSES_NO_SILENCE = ['cero', 'uno', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete', 'ocho', 'nueve']

class Dataset(data.Dataset):
    'Characterizes a dataset for PyTorch'
    def __init__(self, list_IDs, labels, dataframe, discard_silences, experiment_ids, no_weights=False, only_speech_nonspeech=False, gender_classification=False, syllables_classification=False, prosody_position=False, prosody_position_tag='BIL', transform=None, target_transform=None, window_size=.02,
                 window_stride=.01, window_type='hamming', normalize=True, max_len=97):
        'Initialization'
        self.labels = labels
        self.list_IDs = list_IDs
        self.dataframe = dataframe
        self.only_speech_nonspeech = only_speech_nonspeech
        self.discard_silences = discard_silences
        self.gender_classification = gender_classification
        self.syllables_classification = syllables_classification
        self.prosody_position = prosody_position
        if self.gender_classification:
            self.classes = {GENDER_CLASSES[i]: i for i in range(len(GENDER_CLASSES))}
        elif self.syllables_classification:
            if 4 in experiment_ids:
                if discard_silences:
                    self.classes = {SYLLABLES_CORPUS_CLASSES_NO_SILENCE[i]: i for i in range(len(SYLLABLES_CORPUS_CLASSES_NO_SILENCE))}
                else:
                    self.classes = {SYLLABLES_CORPUS_CLASSES[i]: i for i in range(len(SYLLABLES_CORPUS_CLASSES))}
            else:
                if discard_silences:
                    self.classes = {SYLLABLES_DIGITS_CLASSES_NO_SILENCE[i]: i for i in range(len(SYLLABLES_DIGITS_CLASSES_NO_SILENCE))}
                else:
                    self.classes = {SYLLABLES_DIGITS_CLASSES[i]: i for i in range(len(SYLLABLES_DIGITS_CLASSES))}
        elif self.prosody_position:
            if prosody_position_tag == 'BIL':
                self.classes = {PROSODY_POSITION_CLASSES_BIL[i]: i for i in range(len(PROSODY_POSITION_CLASSES_BIL))}
            elif prosody_position_tag == 'BL':
                self.classes = {PROSODY_POSITION_CLASSES_BL[i]: i for i in range(len(PROSODY_POSITION_CLASSES_BL))}
        else:
            if self.only_speech_nonspeech:
                self.classes = {SPEECH_NONSPEECH_CLASSES[i]: i for i in range(len(SPEECH_NONSPEECH_CLASSES))}
            else:
                if self.discard_silences:
                    self.classes = {NUMBERS_CLASSES_NO_SILENCE[i]: i for i in range(len(NUMBERS_CLASSES_NO_SILENCE))}
                else:
                    self.classes = {NUMBERS_CLASSES[i]: i for i in range(len(NUMBERS_CLASSES))}

        # Constants.
        self.PULSEID_SAMPLE_COLUMN = "Sample"
        self.PULSEID_LABEL_COLUMN = "Label"
        self.PULSEID_SUBJECT_ID_COLUMN = "Subject_ID"
        self.PULSEID_SESSION_ID_COLUMN = "Session_ID"
        self.PULSEID_EXPERIMENT_ID_COLUMN = "Experiment_ID"
        self.PULSE_ID_GENDER = "Gender"
        self.PULSE_ID_SYLLABLES_COUNT = "Syllables_Count"
        self.PULSE_ID_PROSODY_POSITION = "Prosody Position"

        self.weight = torch.DoubleTensor(self.get_class_weights(self.classes, self.dataframe, self.only_speech_nonspeech, self.gender_classification, self.syllables_classification, self.prosody_position)) if no_weights is not True else None
        self.transform = transform
        self.target_transform = target_transform
        self.window_size = window_size
        self.window_stride = window_stride
        self.window_type = window_type
        self.normalize = normalize
        self.max_len = max_len

    def __len__(self):
        'Denotes the total number of samples'
        return len(self.list_IDs)

    def __getitem__(self, index):
        'Generates one sample of data'
        # Select sample
        ID = self.list_IDs[index]

        X = self.dataframe.loc[ID][self.PULSEID_SAMPLE_COLUMN]
        y = self.label_to_labelID(self.labels[ID])

        return ID, X, y

    def label_to_labelID(self, label):
        return self.classes[label]

    def get_class_weights(self, classes_dictionary, dataframe, only_speech_nonspeech, gender_classification, syllables_classification, prosody_position):
        classes = []
        for index, row in dataframe.iterrows():
            # If gender classification is done as a function of PPG signal, get the classes as male/female.
            if gender_classification:
                classes.append(row[self.PULSE_ID_GENDER])
            elif syllables_classification:
                classes.append(str(row[self.PULSE_ID_SYLLABLES_COUNT]))
            elif prosody_position:
                classes.append(row[self.PULSE_ID_PROSODY_POSITION])
            # Otherwise, use the labels as the word labeled from .lab files.
            else:
                label = row[self.PULSEID_LABEL_COLUMN]

                if only_speech_nonspeech and (label != "Non-speech"):
                    classes.append("Speech")
                else:
                    classes.append(label)

        # Reset all values of the dictionary to 0.
        classes_counter = dict.fromkeys(classes_dictionary, 0)
        for class_label in classes:
            classes_counter[class_label] += 1

        weights = []
        for key, value in classes_counter.items():
            weights.append(1.0/value)

        return weights