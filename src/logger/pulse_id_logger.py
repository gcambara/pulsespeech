import os

class PulseID_Logger:
    def __init__(self, arguments, date):
        self.directory_path, self.graphs_dir_path, self.log = self.create_log_and_results_directory(arguments, date)
        self.no_verbose_terminal = arguments.no_verbose

    def __del__(self): 
        self.log.close()

    def create_log_and_results_directory(self, arguments, date):
        directory_path = '../results/' + arguments.database_name + '/' + arguments.database_name + '_' + date + '/' + arguments.experiment_id + '/'
        if not os.path.exists(directory_path):
            os.makedirs(directory_path)

        graphs_dir_path = directory_path + 'Graphs/'
        if not os.path.exists(graphs_dir_path):
            os.makedirs(graphs_dir_path)

        log = open(directory_path + 'Log.txt', "w")

        return directory_path, graphs_dir_path, log

    def print(self, information):
        information = str(information)
        self.log.write(information + '\n')

        if not self.no_verbose_terminal:
            print(information)