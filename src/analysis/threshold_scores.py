from sklearn.metrics import *
import numpy as np
import copy

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def convert_scores_to_labels(score_validation, score_test, labels_validation, labels_test, threshold):
    predict_labels_validation = []
    predict_labels_test = []

    for score in score_validation:
        if score < threshold:
            predict_labels_validation.append(0)
        else:
            predict_labels_validation.append(1)

    for score in score_test:
        if score < threshold:
            predict_labels_test.append(0)
        else:
            predict_labels_test.append(1)

    return predict_labels_validation, predict_labels_test


def apply_threshold(score_validation, score_test, labels_validation, labels_test, threshold):
    fpr_validation, tpr_validation, thresholds_validation = roc_curve(labels_validation, score_validation)
    roc_auc = roc_auc_score(labels_validation, score_validation)

    fpr_test, tpr_test, thresholds_test = roc_curve(labels_test, score_test)
    roc_auc_test = roc_auc_score(labels_test, score_test)

    predict_labels_validation = []
    predict_labels_test = []

    for score in score_validation:
        if score < threshold:
            predict_labels_validation.append(0)
        else:
            predict_labels_validation.append(1)

    for score in score_test:
        if score < threshold:
            predict_labels_test.append(0)
        else:
            predict_labels_test.append(1)

    # Compute ROC and confusion matrix for validation
    cnf_matrix_validation = confusion_matrix(labels_validation, predict_labels_validation)
    report_validation = classification_report(labels_validation, predict_labels_validation)  # text report showing the main classification metrics

    # Compute ROC and confusion matrix for test
    print("True labels:")
    print(labels_test)
    print("Predicted labels:")
    print(predict_labels_test)
    cnf_matrix_test = confusion_matrix(labels_test, predict_labels_test)
    report_test = classification_report(labels_test, predict_labels_test)  # text report showing the main classification metrics

    validation_metrics = [fpr_validation, tpr_validation, roc_auc, report_validation, cnf_matrix_validation]
    test_metrics = [fpr_test, tpr_test, roc_auc_test, report_test, cnf_matrix_test]

    return validation_metrics, test_metrics, threshold

def optimal_threshold(score_validation, score_test, labels_validation, labels_test, bound1, bound2):
    fpr_validation, tpr_validation, thresholds_validation = roc_curve(labels_validation, score_validation)
    roc_auc = roc_auc_score(labels_validation, score_validation)

    fpr_test, tpr_test, thresholds_test = roc_curve(labels_test, score_test)
    roc_auc_test = roc_auc_score(labels_test, score_test)
    print('Thresholds test:', thresholds_test)
    print('-------------------------')

    max_precision = 0
    max_recall = 0
    optimal_threshold = 0

    for i in range(0, len(thresholds_test)-1):
        predict_labels_validation = copy.deepcopy(score_validation)
        predict_labels_test = copy.deepcopy(score_test)

        threshold = thresholds_test[i]

        above_threshold = np.where(predict_labels_validation >= threshold)[0]
        below_threshold = np.where(predict_labels_validation < threshold)[0]

        if above_threshold != []:
            for element in above_threshold:
                predict_labels_validation[element] = 1
        if below_threshold != []:
            for element in below_threshold:
                predict_labels_validation[element] = 0

        above_threshold = np.where(predict_labels_test >= threshold)[0]
        below_threshold = np.where(predict_labels_test < threshold)[0]

        if above_threshold != []:
            for element in above_threshold:
                predict_labels_test[element] = 1
        if below_threshold != []:
            for element in below_threshold:
                predict_labels_test[element] = 0

        if fpr_test[i] < bound1:
            # print(bcolors.WARNING + "FPR test:", fpr_test[i], '<', bound1)
            # print("     FPR test:", fpr_test[i])
            # print("     TPR test:", tpr_test[i])
            # print(bcolors.ENDC)
            if tpr_test[i] > max_precision:
                max_recall = tpr_test[i]
                optimal_threshold = threshold
                index = i

        elif fpr_test[i] < bound2 and optimal_threshold == 0:
            # print(bcolors.OKBLUE + "FPR test:",bound1,"<", fpr_test[i], '<',bound2)
            # print("     FPR test:", fpr_test[i])
            # print("     TPR test:", tpr_test[i])
            # print(bcolors.ENDC)
            if tpr_test[i] > max_precision:
                max_recall = tpr_test[i]
                optimal_threshold = threshold
                index = i

        elif optimal_threshold == 0:
            # print(bcolors.OKBLUE + "FPR test:",bound2,"<", fpr_test[i])
            # print("     FPR test:", fpr_test[i])
            # print("     TPR test:", tpr_test[i])
            # print(bcolors.ENDC)
            max_recall = tpr_test[i]
            optimal_threshold = threshold
            index = i
        # print('===========================')
    print('===========================')
    print(bcolors.OKGREEN)
    print('Optimal TH:', optimal_threshold)
    print("FPR test:", fpr_test[index])
    print("TPR test:", tpr_test[index])
    print(bcolors.ENDC)
    print('===========================')

    predict_labels_validation = copy.deepcopy(score_validation)
    predict_labels_test = copy.deepcopy(score_test)

    above_threshold = np.where(predict_labels_validation >= threshold)[0]
    below_threshold = np.where(predict_labels_validation < threshold)[0]

    if above_threshold != []:
        for element in above_threshold:
            predict_labels_validation[element] = 1
    if below_threshold != []:
        for element in below_threshold:
            predict_labels_validation[element] = 0

    above_threshold = np.where(predict_labels_test >= threshold)[0]
    below_threshold = np.where(predict_labels_test < threshold)[0]

    if above_threshold != []:
        for element in above_threshold:
            predict_labels_test[element] = 1
    if below_threshold != []:
        for element in below_threshold:
            predict_labels_test[element] = 0

    # Compute ROC and confusion matrix for validation
    cnf_matrix_validation = confusion_matrix(labels_validation, predict_labels_validation)
    report_validation = classification_report(labels_validation, predict_labels_validation)  # text report showing the main classification metrics

    # Compute ROC and confusion matrix for test
    print("True labels:")
    print(labels_test)
    print("Predicted labels:")
    print(predict_labels_test)
    cnf_matrix_test = confusion_matrix(labels_test, predict_labels_test)
    report_test = classification_report(labels_test, predict_labels_test)  # text report showing the main classification metrics

    validation_metrics = [fpr_validation, tpr_validation, roc_auc, report_validation, cnf_matrix_validation]
    test_metrics = [fpr_test, tpr_test, roc_auc_test, report_test, cnf_matrix_test]

    return validation_metrics, test_metrics, threshold

def compute_metrics(score_validation, score_test, labels_validation, labels_test, threshold):
    fpr_validation, tpr_validation, thresholds_validation = roc_curve(labels_validation, score_validation)
    roc_auc = roc_auc_score(labels_validation, score_validation)

    fpr_test, tpr_test, thresholds_test = roc_curve(labels_test, score_test)
    roc_auc_test = roc_auc_score(labels_test, score_test)

    predict_labels_validation = copy.deepcopy(score_validation)
    predict_labels_test = copy.deepcopy(score_test)

    above_threshold = np.where(predict_labels_validation >= threshold)[0]
    below_threshold = np.where(predict_labels_validation < threshold)[0]

    if above_threshold != []:
        for element in above_threshold:
            predict_labels_validation[element] = 1
    if below_threshold != []:
        for element in below_threshold:
            predict_labels_validation[element] = 0

    above_threshold = np.where(predict_labels_test >= threshold)[0]
    below_threshold = np.where(predict_labels_test < threshold)[0]

    if above_threshold != []:
        for element in above_threshold:
            predict_labels_test[element] = 1
    if below_threshold != []:
        for element in below_threshold:
            predict_labels_test[element] = 0

    # Compute ROC and confusion matrix for validation
    cnf_matrix_validation = confusion_matrix(labels_validation, predict_labels_validation)
    report_validation = classification_report(labels_validation, predict_labels_validation)  # text report showing the main classification metrics

    # Compute ROC and confusion matrix for test
    print("True labels:")
    print(labels_test)
    print("Predicted labels:")
    print(predict_labels_test)
    cnf_matrix_test = confusion_matrix(labels_test, predict_labels_test)
    report_test = classification_report(labels_test, predict_labels_test)  # text report showing the main classification metrics

    validation_metrics = [fpr_validation, tpr_validation, roc_auc, report_validation, cnf_matrix_validation]
    test_metrics = [fpr_test, tpr_test, roc_auc_test, report_test, cnf_matrix_test]

    return validation_metrics, test_metrics, threshold