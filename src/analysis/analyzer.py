from analysis.plot_ROC_and_Report import plot_ROC_and_Report, plot_confusion_matrix
from analysis.threshold_scores import convert_scores_to_labels
from sklearn.metrics import confusion_matrix, roc_auc_score, classification_report, roc_curve
import datetime
import os
import pandas as pd
from matplotlib import pyplot as plt

def analyze_multiclass(arguments, logger, classes, accuracy, valid_pred_labels, valid_labels, test_pred_labels, test_labels):
    # Store configuration file with the execution arguments.
    config_df = store_configuration_file(arguments, logger)

    # Store classification report with precision, recall, F-1 score, support, etc. for every class.
    report_df = store_classification_report(logger, test_labels, test_pred_labels)

    # Store confusion matrix.
    store_confusion_matrix(logger.directory_path + "ConfusionMatrix_Test.png", test_labels, test_pred_labels, classes)

    # Store final results, a dataframe with configuration, classification report info and accuracy.
    store_final_results(logger, config_df, report_df, accuracy)

def analyze_binary_class(arguments, logger, accuracy, valid_pred_scores, valid_labels, test_pred_scores, test_labels):
    # Store configuration file with the execution arguments.
    config_df = store_configuration_file(arguments, logger)

    # Store ROC curve.
    roc_auc = store_roc_curve(logger, logger.directory_path + "ROC_Validation.png", valid_labels, valid_pred_scores, 'PulseID ROC Validation')
    roc_auc = store_roc_curve(logger, logger.directory_path + "ROC_Test.png", test_labels, test_pred_scores, 'PulseID ROC Test')

    # Apply the threshold to the scores to convert them to labels.
    valid_pred_labels, test_pred_labels = convert_scores_to_labels(valid_pred_scores,
                                                                  test_pred_scores,
                                                                  valid_labels,
                                                                  test_labels,
                                                                  arguments.threshold)

    # Store classification report with precision, recall, F-1 score, support, etc. for every class.
    report_df = store_classification_report(logger, test_labels, test_pred_labels)

    # Store confusion matrix.
    store_confusion_matrix(logger.directory_path + "ConfusionMatrix_Test.png", test_labels, test_pred_labels, classes={0, 1})

    # Store final results, a dataframe with configuration, classification report info and accuracy.
    store_final_results(logger, config_df, report_df, accuracy, roc_auc)

def create_results_directory(arguments):
    date = str(datetime.datetime.now())
    date = date.replace(" ", "_")
    directory_path = '../results/' + arguments.database_name + '/' + arguments.database_name + '_' + date + '/' + arguments.experiment_id + '/'
    if not os.path.exists(directory_path):
        os.makedirs(directory_path)

    return directory_path

def extract_dataframe_from_config(config_file_path):
    dataframe = pd.read_csv(config_file_path, sep='^([^,]+),', engine='python')
    dataframe.columns = ["Empty", "Variables", "Values"]
    dataframe = dataframe.set_index('Variables').T
    dataframe = dataframe.drop(index="Empty")
    return dataframe

def store_classification_report(logger, true_labels, predicted_labels):
    report_test = classification_report(true_labels, predicted_labels, output_dict=False)
    logger.print("Saving test report in txt file:")
    logger.print(report_test)
    text_file = open(logger.directory_path + 'Classification_Report.txt', "w")
    text_file.write(report_test)
    text_file.close()

    logger.print("Saving test report in dataframe file:")
    report_test_dict = classification_report(true_labels, predicted_labels, output_dict=True)
    report_df = pd.DataFrame.from_dict(report_test_dict, orient='index')
    report_df.to_csv(logger.directory_path + 'Classification_Report.csv')
    report_df.to_pickle(logger.directory_path + 'Classification_Report.pkl')

    return report_df

def store_configuration_file(arguments, logger):
    config_file_path = logger.directory_path + 'Config.txt'
    logger.print("Saving configuration in txt file:")
    logger.print(config_file_path)
    text_file = open(config_file_path, "w")
    for arg in vars(arguments):
        text_file.write(str(arg) + ", ")
        text_file.write(str(getattr(arguments, arg)))
        text_file.write("\n")
    text_file.close()

    logger.print("Saving configuration in dataframe file:")
    config_df = extract_dataframe_from_config(config_file_path)
    config_df.to_csv(logger.directory_path + 'Config.csv')
    config_df.to_pickle(logger.directory_path + 'Config.pkl')

    return config_df

def store_confusion_matrix(filepath_name, true_labels, predicted_labels, classes):
    cnf_matrix_test = confusion_matrix(true_labels, predicted_labels)
    plot_confusion_matrix(filepath_name, cnf_matrix_test, classes=classes, normalize=True, title='Normalized Test Confusion Matrix')

def store_final_results(logger, config_df, report_df, accuracy, auc=0):
    # Initialize the dataframe from the configuration one.
    dataframe = config_df

    # Add some of the most relevant values from the report one, like average F1-score, precision and recall.
    f1_macro_avg = report_df.loc['macro avg']['f1-score']
    precision_macro_avg = report_df.loc['macro avg']['precision']
    recall_macro_avg = report_df.loc['macro avg']['recall']
    f1_weighted_avg = report_df.loc['weighted avg']['f1-score']
    precision_weighted_avg = report_df.loc['weighted avg']['precision']
    recall_weighted_avg = report_df.loc['weighted avg']['recall']
    dataframe = dataframe.assign(F1_MacroAvg=pd.Series(f1_macro_avg).values)
    dataframe = dataframe.assign(Precision_MacroAvg=pd.Series(precision_macro_avg).values)
    dataframe = dataframe.assign(Recall_MacroAvg=pd.Series(recall_macro_avg).values)
    dataframe = dataframe.assign(F1_WeightedAvg=pd.Series(f1_weighted_avg).values)
    dataframe = dataframe.assign(Precision_WeightedAvg=pd.Series(precision_weighted_avg).values)
    dataframe = dataframe.assign(Recall_WeightedAvg=pd.Series(recall_weighted_avg).values)

    # Finally, add the accuracy and the AUC for binary classification cases.
    dataframe = dataframe.assign(Accuracy=pd.Series(accuracy).values)
    dataframe = dataframe.assign(AUC=pd.Series(auc).values)

    logger.print("Saving final results in dataframe file:")
    dataframe.to_csv(logger.directory_path + "FinalResults.csv")
    dataframe.to_pickle(logger.directory_path + "FinalResults.pkl")

def store_roc_curve(logger, filepath_name, true_labels, pred_scores, suptitle):
    fpr, tpr, thresholds = roc_curve(true_labels, pred_scores)
    roc_auc = roc_auc_score(true_labels, pred_scores)

    plot_ROC_curve(filepath_name, fpr, tpr, roc_auc, suptitle)
    logger.print('-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-')
    logger.print("\nArea Under the Curve (AUC): " + str(roc_auc))
    logger.print('-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-\n')

    return roc_auc

def plot_ROC_curve(filepath_name, fpr, tpr, roc_auc, suptitle):
    fig = plt.figure()
    ax = fig.add_subplot(111, adjustable='box', aspect=1)
    fig, ax = plt.subplots(figsize=(10, 10))
    fig.suptitle(suptitle, fontsize=18, fontweight='bold')

    ax.plot(fpr, tpr, 'b', label='AUC = %0.2f' % roc_auc)
    ax.legend(loc='lower right', fontsize = 16)
    ax.plot([-0.1, 1.1], [-0.1, 1.1], 'r--')
    ax.set_xlabel('False Positive Rate', fontsize = 16)
    ax.set_ylabel('True Positive Rate', fontsize = 16)
    ax.axis([-0.01, 1.01, -0.01, 1.01])

    plt.savefig(filepath_name)
    plt.clf()