# coding=utf-8
# Copyright 2018 jose.fonollosa@upc.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

'''Neural Nets for Speech Prosody Recognition through PPG Signal'''

from __future__ import print_function

import os
import datetime

import pandas as pd
from sklearn.model_selection import train_test_split
import torch
import torch.optim as optim

from analysis.analyzer import analyze_binary_class, analyze_multiclass
from config.arguments import parse_arguments
from data_loader.pulse_id_loader import Dataset
from data_loader import PulseID_Dataframe
from logger import PulseID_Logger
from models import TDNN, VGG, LeNet, LuqNet, StarGANCleaner
from models.train import test, train
from tensorboardX import SummaryWriter

def main():
    # Get the arguments for the current execution.
    args = parse_arguments()

    # Convert the args input experiment IDs to a list, for easier handling.
    experiment_ids = arg_to_list(args.experiment_id)

    # Create the directory for results and logs storage, along with the execution logger.
    date = str(datetime.datetime.now()).replace(" ", "_")
    logger = PulseID_Logger(args, date)
    logger.print(args)

    # Create a Tensorboard summary writer for logging the evolution of variables as well.
    writer = SummaryWriter(logger.directory_path + '/TensorBoard')

    # Determine if CUDA is being used, and how many GPUs is going to use if so.
    args.cuda = args.cuda and torch.cuda.is_available()
    torch.manual_seed(args.seed)
    if args.cuda:
        torch.cuda.manual_seed(args.seed)
        logger.print('Using CUDA with {0} GPUs'.format(torch.cuda.device_count()))

    # Get the dataframe for the whole data set. If a path for the pickled dataframe has been specified,
    # take the dataframe from it. Otherwise, generate the dataframe from the raw data.
    pid = PulseID_Dataframe(args)
    if args.dataframe_pickle_path != '':
        pid_dataframe = pd.read_pickle(args.dataframe_pickle_path)
    else:
        pid_dataframe = pid.generate_pulseID_dataframe(args, experiment_ids)
    pid_dataframe.to_csv("PulseID_DF.csv")

    pid_dataframe = pid.process_pulseID_dataframe(args=args, dataframe=pid_dataframe, experiment_ids=experiment_ids)
    pid_dataframe.to_csv("Processed_DF.csv")

    #pid_dataframe = pd.read_pickle('Cleaned_DF_Model003.pkl')
    mean_word_time, std_word_time, min_word_time, max_word_time = pid.compute_mean_word_time(pid_dataframe, experiment_ids, discard_silences=args.discard_silences, include_catalan=args.include_catalan, include_noisy_wavs=args.include_noisy_wavs)
    logger.print('Average word duration: {:.4f}, Std. Deviation: {}\n'.format(mean_word_time, std_word_time))
    logger.print('Minimum word duration: {:.4f}, Maximum word duration: {}\n'.format(min_word_time, max_word_time))

    # Adjust the extracted dataframe to the desired time window.
    pid.data_visualization(args=args, dataframe=pid_dataframe, experiment_ids=experiment_ids, logger=logger)

    # Split the data set in training and test dataframes, and then the training set in training and validation dataframes.
    if args.variable_splitting_seed:
        train_dataframe, test_dataframe = train_test_split(pid_dataframe, test_size=0.2)
        train_dataframe, validation_dataframe = train_test_split(train_dataframe, test_size=0.2)
    else:
        train_dataframe, test_dataframe = train_test_split(pid_dataframe, test_size=0.2, random_state=args.seed)
        train_dataframe, validation_dataframe = train_test_split(train_dataframe, test_size=0.2, random_state=args.seed)

    # Create a dictionary with samples and labels.
    partition = pid.get_partition(train_dataframe, validation_dataframe, test_dataframe)
    labels = pid.get_labeled_samples(pid_dataframe, only_speech_nonspeech=args.only_speech_nonspeech, gender_classification=args.gender_classification, syllables_classification=args.syllables_classification, prosody_position=args.prosody_position)

    # Loading data.
    if args.train:
        train_dataset = Dataset(list_IDs=partition['train'], labels=labels, dataframe=train_dataframe, discard_silences=args.discard_silences, experiment_ids=experiment_ids, no_weights=args.no_weights, only_speech_nonspeech=args.only_speech_nonspeech, gender_classification=args.gender_classification, syllables_classification=args.syllables_classification, prosody_position=args.prosody_position, prosody_position_tag=args.prosody_position_tag, window_size=args.window_size, window_stride=args.window_stride,
            window_type=args.window_type, normalize=args.normalize)
        train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=args.batch_size, shuffle=True,
            num_workers=args.num_workers, pin_memory=args.cuda, sampler=None)

        valid_dataset = Dataset(list_IDs=partition['validation'], labels=labels, dataframe=validation_dataframe, discard_silences=args.discard_silences, experiment_ids=experiment_ids, no_weights=args.no_weights, only_speech_nonspeech=args.only_speech_nonspeech, gender_classification=args.gender_classification, syllables_classification=args.syllables_classification, prosody_position=args.prosody_position, prosody_position_tag=args.prosody_position_tag, window_size=args.window_size, window_stride=args.window_stride,
            window_type=args.window_type, normalize=args.normalize)
        valid_loader = torch.utils.data.DataLoader(valid_dataset, batch_size=args.batch_size, shuffle=None,
            num_workers=args.num_workers, pin_memory=args.cuda, sampler=None)

        # Build model.
        if args.arc.startswith('VGG'):
            model = VGG(args, num_classes=len(train_dataset.classes), sample_size=args.sampling_rate*args.time_window)
        elif args.arc == 'TDNN':
            model = TDNN(args, num_classes=len(train_dataset.classes), sample_size=args.sampling_rate*args.time_window)
        elif args.arc == 'LeNet':
            model = LeNet(args, num_classes=len(train_dataset.classes), sample_size=args.sampling_rate*args.time_window)
        elif args.arc == 'LuqNet':
            model = LuqNet(args, num_classes=len(train_dataset.classes), sample_size=args.sampling_rate*args.time_window)
        elif args.arc == 'StarGANCleaner':
            model = StarGANCleaner()

        if args.cuda:
            model = torch.nn.DataParallel(model).cuda()

        # Print model's summary.
        logger.print(model)

        # Define optimizer.
        if args.optimizer.lower() == 'adam':
            optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)
        else:
            optimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=args.momentum, weight_decay=args.weight_decay)

            if args.optimizer.lower() == 'sgdr':
                scheduler = optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=int(args.epochs/4))

        checkpoint_folder = args.checkpoint + "/" + date
        best_valid_metric = 0
        iteration = 0
        epoch = 1

        # Training with early stopping.
        while (epoch < args.epochs + 1) and (iteration < args.patience):
            if args.optimizer.lower() == 'sgdr':
                scheduler.step()
            train(args, train_loader, model, optimizer, epoch, args.cuda, args.log_interval, logger,
                weight=train_dataset.weight)
            if args.arc != 'StarGANCleaner':
                valid_loss, valid_acc, valid_metric, valid_labels, valid_pred_labels, valid_pred_scores = test(valid_loader, model, args.performance_metric, args.cuda, logger, data_set='Validation')
            else:
                valid_loss = epoch
                valid_acc = epoch
                valid_metric = epoch
            if not os.path.isdir(checkpoint_folder):
                if not os.path.isdir(args.checkpoint):
                    os.mkdir(args.checkpoint)
                os.mkdir(checkpoint_folder)
            torch.save(model.module if args.cuda else model,
                './{}/{}/model{:03d}.t7'.format(args.checkpoint, date, epoch))
            if valid_metric <= best_valid_metric:
                iteration += 1
                logger.print(args.performance_metric + ' was not improved, iteration {0}'.format(str(iteration)))
            else:
                logger.print('Saving state')
                iteration = 0
                best_valid_metric = valid_metric
                state = {
                    'valid_metric': valid_metric,
                    'valid_loss': valid_loss,
                    'epoch': epoch,
                }
                if not os.path.isdir(checkpoint_folder):
                    if not os.path.isdir(args.checkpoint):
                        os.mkdir(args.checkpoint)
                    os.mkdir(checkpoint_folder)
                torch.save(state, './{}/{}/ckpt.t7'.format(args.checkpoint, date))

            # Visualization for scalar training values.
            writer.add_scalar('Validation/' + str(args.performance_metric), valid_metric, epoch)
            writer.add_scalar('Validation/Accuracy', valid_acc, epoch)
            writer.add_scalar('Validation/Loss', valid_loss, epoch)
            # Visualization for weights and biases in the model's layers.
            for name, parameter in model.named_parameters():
                writer.add_histogram('Model/' + args.arc + '/' + name, parameter.clone().cpu().data.numpy(), epoch)

            # If SGD with Restarts is chosen as the optimizer, reset the learning rate to the initial value
            # when reaching the minimum learning rate.
            if (args.optimizer.lower() == 'sgdr') and (epoch % int(args.epochs/4) == 0):
                scheduler = optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=int(args.epochs/4))
            epoch += 1

    # Test model.
    if args.arc != 'StarGANCleaner':
        test_dataset = Dataset(list_IDs=partition['test'], labels=labels, dataframe=test_dataframe, discard_silences=args.discard_silences, experiment_ids=experiment_ids, no_weights=args.no_weights, only_speech_nonspeech=args.only_speech_nonspeech, gender_classification=args.gender_classification, syllables_classification=args.syllables_classification, prosody_position=args.prosody_position, prosody_position_tag=args.prosody_position_tag, window_size=args.window_size, window_stride=args.window_stride,
            window_type=args.window_type, normalize=args.normalize)
        test_loader = torch.utils.data.DataLoader(
            test_dataset, batch_size=args.test_batch_size, shuffle=None,
            num_workers=args.num_workers, pin_memory=args.cuda, sampler=None)

        state = torch.load('./{}/{}/ckpt.t7'.format(args.checkpoint, date))
        epoch = state['epoch']
        logger.print("Testing model {} (epoch {})".format(args.checkpoint, epoch))
        model = torch.load('./{}/{}/model{:03d}.t7'.format(args.checkpoint, date, epoch))
        if args.cuda:
            model = torch.nn.DataParallel(model).cuda()

        # Keep the optimal model used for testing, delete the other ones just to free space, if configured in options.
        if args.save_models != True:
            for root, dirs, files in os.walk('./{}'.format(checkpoint_folder)):
                for file in files:
                    if (file != 'ckpt.t7') and (file != 'model{:03d}.t7'.format(epoch)):
                        os.remove(os.path.join(root, file))

            results = './{}/{}/{}.csv'.format(args.checkpoint, date, os.path.basename(args.test_path))
            logger.print("Saving results in {}".format(results))
            test_loss, test_acc, test_metric, test_labels, test_pred_labels, test_pred_scores = test(test_loader, model, args.performance_metric, args.cuda, logger, save=results)

            if len(test_dataset.classes) != 2:
                analyze_multiclass(args, logger, test_dataset.classes, test_acc, valid_pred_labels, valid_labels, test_pred_labels, test_labels)
            else:
                analyze_binary_class(args, logger, test_acc, valid_pred_scores, valid_labels, test_pred_scores, test_labels)

    # Close TensorBoard's summary writer.
    writer.close()

def arg_to_list(argument):
    argument_map = map(int, argument.strip('[]').split(','))
    argument_list = []
    for value in argument_map:
        argument_list.append(value)
    return argument_list

if __name__ == '__main__':
    main()