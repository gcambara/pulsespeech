# coding=utf-8
# Copyright 2018 jose.fonollosa@upc.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Models for Speech Prosody Recognition through PPG Signal."""

import math
import torch
import torch.nn as nn
import torch.nn.functional as F

class TDNN(nn.Module):
    def __init__(self, args, num_classes=2, sample_size=200):
        super(TDNN, self).__init__()
        self.tdnn = nn.Sequential(
            nn.Conv1d(1, args.tdnn_filter_number, stride=1, dilation=1, kernel_size=3),
            nn.ReLU(True),
            nn.Conv1d(args.tdnn_filter_number, args.tdnn_filter_number, stride=1, dilation=1, kernel_size=4),
            nn.ReLU(True),
            nn.Conv1d(args.tdnn_filter_number, args.tdnn_filter_number, stride=1, dilation=3, kernel_size=3),
            nn.ReLU(True),
            nn.Conv1d(args.tdnn_filter_number, args.tdnn_filter_number, stride=1, dilation=3, kernel_size=3),
            nn.ReLU(True),
            nn.Conv1d(args.tdnn_filter_number, args.tdnn_filter_number, stride=1, dilation=3, kernel_size=3),
            nn.ReLU(True),
            nn.Conv1d(args.tdnn_filter_number, args.tdnn_filter_number, stride=1, dilation=3, kernel_size=3),
            nn.ReLU(True),
            nn.Conv1d(args.tdnn_filter_number, args.tdnn_filter_number, stride=1, dilation=3, kernel_size=3),
            nn.ReLU(True),
            nn.MaxPool1d(3, stride=3),
        )

        self.classifier = nn.Sequential(
            nn.Linear(compute_cnn_output_size(self.tdnn, sample_size), args.tdnn_classifier_neurons),
            nn.ReLU(True),
            nn.Dropout(),
            nn.Linear(args.tdnn_classifier_neurons, args.tdnn_classifier_neurons),
            nn.ReLU(True),
            nn.Dropout(),
            nn.Linear(args.tdnn_classifier_neurons, num_classes),
        )
        self._initialize_weights()

    def forward(self, x):
        x.unsqueeze_(1)
        x = self.tdnn(x)
        x = x.view(x.size(0), -1)
        x = self.classifier(x)
        return x

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
                if m.bias is not None:
                    m.bias.data.zero_()
            elif isinstance(m, nn.Conv1d):
                n = m.kernel_size[0] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
                if m.bias is not None:
                    m.bias.data.zero_()
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                m.weight.data.normal_(0, 0.01)
                m.bias.data.zero_()


class LeNet(nn.Module):
    def __init__(self, args, num_classes=2, sample_size=200):
        super(LeNet, self).__init__()
        self.maxpool_kernel_size = args.leNet_kernel_size_maxpool
        self.conv1 = nn.Conv1d(1, args.leNet_filter_number, kernel_size=args.leNet_kernel_size_conv)
        self.conv2 = nn.Conv1d(args.leNet_filter_number, args.leNet_filter_number, kernel_size=args.leNet_kernel_size_conv)
        self.conv2_drop = nn.Dropout()
        self.fc1 = nn.Linear(compute_cnn_output_size(nn.Sequential(self.conv1, nn.MaxPool1d(kernel_size=self.maxpool_kernel_size), 
                                                                   self.conv2, nn.MaxPool1d(kernel_size=self.maxpool_kernel_size)), sample_size), 
                                                                   args.leNet_classifier_neurons)
        self.fc2 = nn.Linear(args.leNet_classifier_neurons, num_classes)

    def forward(self, x):
        x.unsqueeze_(1)
        x = F.relu(F.max_pool1d(self.conv1(x), self.maxpool_kernel_size))
        x = F.relu(F.max_pool1d(self.conv2_drop(self.conv2(x)), self.maxpool_kernel_size))
        x = x.view(x.size(0), -1)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        return x


class VGG(nn.Module):
    def __init__(self, args, num_classes=2, sample_size=200):
        super(VGG, self).__init__()
        self.features = make_layers(args, sample_size)
        self.classifier = nn.Sequential(
            nn.Linear(compute_cnn_output_size(self.features, sample_size), args.vgg_classifier_neurons),
            nn.ReLU(True),
            nn.Dropout(),
            nn.Linear(args.vgg_classifier_neurons, args.vgg_classifier_neurons),
            nn.ReLU(True),
            nn.Dropout(),
            nn.Linear(args.vgg_classifier_neurons, num_classes),
        )
        self._initialize_weights()

    def forward(self, x):
        x.unsqueeze_(1)
        x = self.features(x)
        x = x.view(x.size(0), -1)
        x = self.classifier(x)
        return x

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv1d):
                n = m.kernel_size[0] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
                if m.bias is not None:
                    m.bias.data.zero_()
            elif isinstance(m, nn.BatchNorm1d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                m.weight.data.normal_(0, 0.01)
                m.bias.data.zero_()

class LuqNet(nn.Module):
    def __init__(self, args, num_classes=2, sample_size=200):
        super(LuqNet, self).__init__()
        self.CONVOLUTIONS_NUMBER = 3
        self.CONCATENATION_DIMENSION = 1
        self.conv_1 = nn.Sequential(nn.Conv1d(in_channels=1, out_channels=args.luqNet_filter_number, kernel_size=50), nn.ReLU(), 
                                     nn.MaxPool1d(kernel_size=compute_conv_layer_output_size(sample_size, kernel_size=50)))
        self.conv_2 = nn.Sequential(nn.Conv1d(in_channels=1, out_channels=args.luqNet_filter_number, kernel_size=30), nn.ReLU(), 
                                     nn.MaxPool1d(kernel_size=compute_conv_layer_output_size(sample_size, kernel_size=30)))
        self.conv_3 = nn.Sequential(nn.Conv1d(in_channels=1, out_channels=args.luqNet_filter_number, kernel_size=20), nn.ReLU(), 
                                     nn.MaxPool1d(kernel_size=compute_conv_layer_output_size(sample_size, kernel_size=20)))
        self.classifier = nn.Sequential(
            nn.Linear(args.luqNet_filter_number*self.CONVOLUTIONS_NUMBER, args.luqNet_classifier_neurons),
            nn.ReLU(True),
            nn.Dropout(),
            nn.Linear(args.luqNet_classifier_neurons, num_classes),
        )
        self._initialize_weights()

    def forward(self, x):
        x.unsqueeze_(1)
        x = torch.cat((self.conv_1(x), self.conv_2(x), self.conv_3(x)), self.CONCATENATION_DIMENSION)
        x = x.view(x.size(0), -1)
        x = self.classifier(x)
        return x

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv1d):
                n = m.kernel_size[0] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
                if m.bias is not None:
                    m.bias.data.zero_()
            elif isinstance(m, nn.BatchNorm1d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                m.weight.data.normal_(0, 0.01)
                m.bias.data.zero_()

#Helper functions to create a 1D autoencoder borrowed from https://github.com/eveningglow/StarGAN-pytorch/blob/master/model.py
class StarGANCleaner(nn.Module):
    def __init__(self, ):
        super(StarGANCleaner, self).__init__()
        self.encoder = nn.Sequential(
            ConvolutionalBlock(in_dim=1, out_dim=32, k=5, s=1, p=2), #maintains input shape and creates 64 channels
            ConvolutionalBlock(in_dim=32, out_dim=64, k=4, s=2, p=1)
        )
        self.residuals = nn.Sequential(
            ResBlock(64,64)
        )
        self.decoder = nn.Sequential(
            DeconvBlock(64, 32, k=4, s=2, p=1),
            ConvolutionalBlock(in_dim=32, out_dim=1, k=5, s=1, p=2, non_linear='none') #Maintains input shape and combines into 1 channel
        )

        self.loss_function = nn.L1Loss()
        self.normalize=True
        self.normalization = nn.InstanceNorm1d(1)

    def forward(self, x):
        """Write forward function encode -> residualblocks -> decode."""
        x.unsqueeze_(1)
        x = self.encoder(x)
        x = self.residuals(x)
        x = self.decoder(x)
        return x

    def loss(self, preds, y):
        return self.loss_function(self.normalization(preds), self.normalization(y))

class ConvolutionalBlock(nn.Module):
    def __init__(self, in_dim, out_dim, k=4, s=2, p=1, norm=True, non_linear='relu'):
        super(ConvolutionalBlock, self).__init__()
        layers = []
        layers += [nn.Conv1d(in_dim, out_dim, kernel_size=k, stride=s, padding=p)]

        if norm is True:
            layers += [nn.InstanceNorm1d(out_dim, affine=True)]
            
        if non_linear == 'relu':
            layers += [nn.ReLU()]
        elif non_linear == 'leaky_relu':
            layers += [nn.LeakyReLU()]
        elif non_linear == 'tanh':
            layers += [nn.Tanh()]
            
        self.conv_block = nn.Sequential(* layers)
        
    def forward(self, x):
        out = self.conv_block(x)
        return out
        
class ResBlock(nn.Module):
    def __init__(self, in_dim, internal_dim, k=3, s=1, p=1):
        super(ResBlock, self).__init__()
        
        # Use 2 ConvolutionalBlock in 1 ResBlock
        conv_block_1 = ConvolutionalBlock(in_dim, internal_dim, k=k, s=s, p=p, 
                                 norm=True, non_linear='relu')
        conv_block_2 = ConvolutionalBlock(internal_dim, in_dim, k=k, s=s, p=p, 
                                 norm=True, non_linear=None)
        self.res_block = nn.Sequential(conv_block_1, conv_block_2)
    
    def forward(self, x):
        out = x + self.res_block(x)
        return out
    
class DeconvBlock(nn.Module):
    def __init__(self, in_dim, out_dim, k=3, s=1, p=1):
        super(DeconvBlock, self).__init__()
        self.deconv_block = nn.Sequential(
                                nn.ConvTranspose1d(in_dim, out_dim, kernel_size=k, stride=s, padding=p),
                                nn.InstanceNorm1d(out_dim, affine=True),
                                nn.ReLU()
                            )

    def forward(self, x):
        out = self.deconv_block(x)
        return out

def compute_conv_layer_output_size(input_size, kernel_size, stride=1, padding=0, dilation=1):
    """Computes the output size of a tensor after passing through a convolutional layer."""
    return int((((input_size + 2*padding - dilation*(kernel_size - 1) - 1)/stride) + 1))

def compute_cnn_output_size(cnn_network, input_size):
    """Computes the output size of a tensor after passing through a convolutional neural network."""
    cnn_output_size = input_size
    channels = 1
    for layer in cnn_network:
        if isinstance(layer, nn.Conv1d):
            channels = layer.out_channels
            input_size = compute_conv_layer_output_size(input_size, kernel_size=layer.kernel_size[0], 
                                                                    padding=layer.padding[0], 
                                                                    stride=layer.stride[0],
                                                                    dilation=layer.dilation[0])
            cnn_output_size = channels*input_size
        elif isinstance(layer, nn.MaxPool1d):
            input_size = compute_conv_layer_output_size(input_size, kernel_size=layer.kernel_size, 
                                                                    padding=layer.padding, 
                                                                    stride=layer.stride,
                                                                    dilation=layer.dilation)
            cnn_output_size = channels*input_size

    return cnn_output_size

def get_conv_layer_cfg(arguments):
    """Returns the configuration of convolutional layers input through execution arguments, used for customized configurations."""
    vgg_custom_conv_map = map(str, arguments.vgg_custom_conv.strip('[]').split(','))
    vgg_custom_conv = []
    for layer in vgg_custom_conv_map:
        # If the current layer is a maxpooling ('M'), store it as a string.
        if layer == 'M':
            vgg_custom_conv.append(layer)
        # Otherwise it shall indicate the number of filters, store it as an int.
        else:
            vgg_custom_conv.append(int(layer))

    return vgg_custom_conv

def make_layers(args, input_size, batch_norm=True):
    """Creates a PyTorch Sequential object of convolutional and/or Maxpool layers, from a given configuration list."""
    layers = []
    in_channels = 1

    # If a custom convolutional layer configuration has been set from command line, use that one.
    if args.vgg_custom_conv != '':
        convolutional_layers_distribution = get_conv_layer_cfg(args)
    # Otherwise, use the layers configurations specified in the architecture parameter.
    else:
        convolutional_layers_distribution = cfg[args.arc]

    for v in convolutional_layers_distribution:
        if v == 'M':
            layers += [nn.MaxPool1d(kernel_size=args.kernel_size_maxpool, stride=args.stride_maxpool, padding=args.padding_maxpool)]
        else:
            conv1d = nn.Conv1d(in_channels, v, kernel_size=args.kernel_size_conv, padding=args.padding_conv, stride=args.stride_conv)
            if batch_norm:
                layers += [conv1d, nn.BatchNorm1d(v), nn.ReLU(inplace=True)]
            else:
                layers += [conv1d, nn.ReLU(inplace=True)]
            in_channels = v
    return nn.Sequential(*layers)

cfg = {
    'VGG11': [64, 'M', 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
    'VGG13': [64, 64, 'M', 128, 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
    'VGG16': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'M', 512, 512, 512, 'M', 512, 512, 512, 'M'],
    'VGG19': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 256, 'M', 512, 512, 512, 512, 'M', 512, 512, 512, 512, 'M'],
}
