# coding=utf-8
# Copyright 2018 jose.fonollosa@upc.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function, division
import torch
import torch.nn as nn
from torch.autograd import Variable

from sklearn.metrics import f1_score 
from collections import Counter


def train(args, loader, model, optimizer, epoch, cuda, log_interval, logger, weight=None):
    model.train()
    model.double()
    global_epoch_loss = 0
    for batch_idx, (_, data, target) in enumerate(loader):
        criterion = nn.CrossEntropyLoss(weight=weight)
        if cuda:
            data, target = data.cuda(), target.cuda()
            criterion = criterion.cuda()
        data, target = Variable(data), Variable(target)
        optimizer.zero_grad()
        output = model(data)
        if args.arc == 'StarGANCleaner':
            loss = model.loss(output, data)
        else:
            loss = criterion(output, target)
        loss.backward()
        optimizer.step()
        global_epoch_loss += loss.item()
        if batch_idx % log_interval == 0:
            logger.print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(loader.dataset), 100. * batch_idx / len(loader), loss.item()))
    return global_epoch_loss / len(loader.dataset)

def test(loader, model, performance_metric, cuda, logger, data_set='Test', save=None):
    model.eval()
    test_loss = 0
    correct = 0
    true_labels = []
    predicted_labels = []
    predicted_scores = []
    sample_IDs = []

    if save is not None:
        csv = open(save, 'wt')
        print('Sample_ID,True_Label,Predicted_Label', file=csv)

    with torch.no_grad():
        for keys, data, target in loader:
            criterion = nn.CrossEntropyLoss(reduction='sum')
            if cuda:
                data, target = data.cuda(), target.cuda()
                criterion = criterion.cuda()
            data, target = Variable(data), Variable(target)
            output = model(data)
            test_loss += criterion(output, target).item()  # sum up batch loss
            pred = output.data.max(1, keepdim=True)[1]  # get the index of the max log-probability
            score = output.data[:,1] - output.data[:,0]
            correct += pred.eq(target.data.view_as(pred)).cpu().sum()

            true_labels += target.data.view_as(pred).squeeze().tolist()
            predicted_labels += pred.squeeze().tolist()
            predicted_scores += score.squeeze().tolist()
            sample_IDs += keys.squeeze().tolist()

    if save is not None:
        for i, key in enumerate(true_labels):
            print(str(sample_IDs[i]) + ','+str(true_labels[i])+','+str(predicted_labels[i]), file=csv)

    test_loss /= len(loader.dataset)
    accuracy = correct.item() / len(loader.dataset)

    if performance_metric != 'accuracy':
        metric = get_performance_metric(performance_metric, true_labels, predicted_labels)
        logger.print('\n{} set: Average loss: {:.4f}, Accuracy: {}/{} ({:.1f}%), {}: {:.4f}\n'.format(
            data_set, test_loss, correct, len(loader.dataset), 100 * accuracy, performance_metric, metric))
    else:
        metric = accuracy
        logger.print('\n{} set: Average loss: {:.4f}, Accuracy: {}/{} ({:.1f}%)\n'.format(
            data_set, test_loss, correct, len(loader.dataset), 100 * accuracy))

    return test_loss, accuracy, metric, true_labels, predicted_labels, predicted_scores

def get_performance_metric(performance_metric, true_labels, predicted_labels):
    if performance_metric == 'f1_weighted_avg':
        metric = f1_score(true_labels, predicted_labels, average='weighted')
    elif performance_metric == 'f1_macro_avg':
        metric = f1_score(true_labels, predicted_labels, average='macro')

    return metric